import sys

import numpy as np
import pandas as pd
import matplotlib

matplotlib.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns

import simulator.simulation as sim
import simulator.agent as agent
import simulator.network as network
import simulator.simulation_runner as sim_runner
import simulator.patient_model as pmodel
import simulator.distruption as disr
import simulator.decision_maker as dmaker
from simulator.order import *
from simulator.agent import *


simulation = sim.Simulation()

agent_builder = agent.AgentBuilder()
agent_builder.lead_time = 2
agent_builder.review_time = 0
agent_builder.cycle_service_level = 0.9
agent_builder.history_preserve_time = 20
# agent_builder.fixed_order_up_to_level = True

mn1 = agent_builder.build("manufacturer")
ds1 = agent_builder.build("distributor")
hc1 = agent_builder.build("health_center")

hc1.upstream_nodes.extend([ds1])
ds1.upstream_nodes.extend([mn1])
ds1.downstream_nodes.extend([hc1])
mn1.downstream_nodes.extend([ds1])

simulation.add_agent(mn1)
simulation.add_agent(ds1)
simulation.add_agent(hc1)

net = network.Network(3)
info_net = network.Network(3)
for i in range(3):
    for j in range(3):
        net.connectivity[i, j] = 1
        info_net.connectivity[i, j] = 0
simulation.network = net
simulation.info_network = info_net

## Decision Makers
decision_maker = dmaker.PerAgentDecisionMaker()

hc1_dmaker = dmaker.SimpleHCDecisionMaker(hc1)
decision_maker.add_decision_maker(hc1_dmaker)

ds1_dmaker = dmaker.SimpleDSDecisionMaker(ds1)
decision_maker.add_decision_maker(ds1_dmaker)

mn1_dmaker = dmaker.TempMNDecisionMaker(mn1)
# mn1_dmaker = dmaker.SimpleMNDecisionMaker(mn1)
decision_maker.add_decision_maker(mn1_dmaker)

patient_model = pmodel.ConstantPatientModel([hc1])

simulation.patient_model = patient_model

# total_capacity = 2 * (patient_model.non_urgent)
total_capacity = 4 * patient_model.non_urgent

beta = 0.5
mn1.line_capacity = 20
mn1.capacity = beta * total_capacity
mn1.num_of_lines = int(mn1.capacity / mn1.line_capacity)
mn1.num_active_lines = mn1.num_of_lines

# spike in demand disruption
spike_demand = disr.DemandChangeDisruption(patient_model)
spike_demand.start_time = 120
spike_demand.end_time = 130
spike_demand.change = 120
simulation.disruptions.append(spike_demand)

runner = sim_runner.SimulationRunner(simulation, decision_maker)

data_columns = ['time', 'agent', 'item', 'value', 'unit']
data = pd.DataFrame(columns=data_columns)

for i in range(300):
    print('Cycle ' + str(i) + '...')
    runner.next_cycle()

    if i > 0:
        data = data.append(pd.DataFrame(hc1.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(ds1.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(mn1.collect_data(i), columns=data_columns))

data.reset_index()
data.to_csv('results_op_ed.csv')
print data
hc1_data = data[data['agent'] == 'hc_2']
plt.figure()
sns.tsplot(hc1_data, time='time', condition='item', value='value',
           unit='unit').set_title('Health Center 1')
plt.savefig('hc1.png', dpi=600)

ds1_data = data[data['agent'] == 'ds_1']
plt.figure()
sns.tsplot(ds1_data, time='time', condition='item',
           value='value', unit='unit').set_title('Distributor 1')
plt.savefig('ds1.png', dpi=600)

mn1_data = data[data['agent'] == 'mn_0']
plt.figure()
sns.tsplot(mn1_data, time='time', condition='item',
           value='value', unit='unit').set_title('Manufacture 1')
plt.savefig('mn1.png', dpi=600)
