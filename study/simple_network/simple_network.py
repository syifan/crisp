"""
Only simulator example for both education and testing purposes
"""
import sys

import numpy as np
import pandas as pd
import matplotlib

matplotlib.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns

import simulator.simulation as sim
import simulator.agent as agent
import simulator.network as network
import simulator.simulation_runner as sim_runner
import simulator.patient_model as pmodel
import simulator.distruption as disr
import simulator.decision_maker as dmaker
from simulator.order import *
from simulator.agent import *


simulation = sim.Simulation()

agent_builder = agent.AgentBuilder()
agent_builder.lead_time = 2
agent_builder.review_time = 0
agent_builder.cycle_service_level = 0.9
agent_builder.history_preserve_time = 20
# agent_builder.fixed_order_up_to_level = True

mn1 = agent_builder.build("manufacturer")
mn2 = agent_builder.build("manufacturer")
ds1 = agent_builder.build("distributor")
ds2 = agent_builder.build("distributor")
hc1 = agent_builder.build("health_center")
hc2 = agent_builder.build("health_center")

hc1.upstream_nodes.extend([ds1, ds2])
hc2.upstream_nodes.extend([ds1, ds2])
ds1.upstream_nodes.extend([mn1, mn2])
ds2.upstream_nodes.extend([mn1, mn2])
ds1.downstream_nodes.extend([hc1, hc2])
ds2.downstream_nodes.extend([hc1, hc2])
mn1.downstream_nodes.extend([ds1, ds2])
mn2.downstream_nodes.extend([ds1, ds2])

simulation.add_agent(mn1)
simulation.add_agent(mn2)
simulation.add_agent(ds1)
simulation.add_agent(ds2)
simulation.add_agent(hc1)
simulation.add_agent(hc2)

net = network.Network(6)
info_net = network.Network(6)
for i in range(6):
    for j in range(6):
        net.connectivity[i, j] = 1
        info_net.connectivity[i, j] = 0
simulation.network = net
simulation.info_network = info_net

## Decision Makers
decision_maker = dmaker.PerAgentDecisionMaker()

hc1_dmaker = dmaker.SimpleHCDecisionMaker(hc1, 'equally')
decision_maker.add_decision_maker(hc1_dmaker)

hc2_dmaker = dmaker.SimpleHCDecisionMaker(hc2, 'bytrust')
decision_maker.add_decision_maker(hc2_dmaker)

ds1_dmaker = dmaker.SimpleDSDecisionMaker(ds1)
decision_maker.add_decision_maker(ds1_dmaker)

ds2_dmaker = dmaker.SimpleDSDecisionMaker(ds2)
decision_maker.add_decision_maker(ds2_dmaker)

mn1_dmaker = dmaker.SimpleMNDecisionMaker(mn1)
decision_maker.add_decision_maker(mn1_dmaker)

mn2_dmaker = dmaker.SimpleMNDecisionMaker(mn2)
decision_maker.add_decision_maker(mn2_dmaker)

patient_model = pmodel.DifferentPatientModel([hc1, hc2])

simulation.patient_model = patient_model

total_capacity = 2 * (patient_model.non_urgent_1 + patient_model.non_urgent_2)

beta = 0.5
mn1.line_capacity = 20
mn1.capacity = beta * total_capacity
mn1.num_of_lines = int(mn1.capacity / mn1.line_capacity)
mn1.num_active_lines = mn1.num_of_lines

mn2.line_capacity = 20
mn2.capacity = (1 - beta) * total_capacity
mn2.num_of_lines = int(mn2.capacity / mn2.line_capacity)
mn2.num_active_lines = mn2.num_of_lines

# disruption
manufacturer_shutdown = disr.LineShutDownDisruption(simulation, 40)
manufacturer_shutdown.manufacturer_id = 1
manufacturer_shutdown.happen_day_1 = 70
manufacturer_shutdown.end_day_1 = 80
manufacturer_shutdown.decrease_factor_1 = 0.95
simulation.disruptions.append(manufacturer_shutdown)

runner = sim_runner.SimulationRunner(simulation, decision_maker)

data_columns = ['time', 'agent', 'item', 'value', 'unit']
data = pd.DataFrame(columns=data_columns)

for i in range(250):
    print('Cycle ' + str(i) + '...')
    runner.next_cycle()

    if i > 0:
        data = data.append(pd.DataFrame(hc1.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(hc2.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(ds1.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(ds2.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(mn1.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(mn2.collect_data(i), columns=data_columns))

data.reset_index()
data.to_csv('results.csv')

# Healthcenter 1
hc1_data = data[data['agent'] == 'hc_4']
hc1_data.set_index('time', inplace=True)
plt.figure()
plt.plot(hc1_data[hc1_data['item'] == 'inventory']['value'], color='navy', label='Inventory')
plt.plot(hc1_data[hc1_data['item'] == 'demand']['value'], color='orange', label='Demand')
plt.plot(hc1_data[hc1_data['item'] == 'backlog']['value'], color='forestgreen', label='Backlog')
plt.plot(hc1_data[hc1_data['item'] == 'on-order']['value'], color='firebrick', label='On-order')
plt.legend(loc='upper right')
plt.title('Healthcenter1 State')
plt.savefig('hc1_state.png', dpi=600)
plt.figure()
plt.plot(hc1_data[hc1_data['item'] == 'order-ds2']['value'], color='orange', label='Order to DS1')
plt.plot(hc1_data[hc1_data['item'] == 'order-ds3']['value'], color='forestgreen', label='Order to DS2')
plt.legend(loc='upper right')
plt.title('Healthcenter1 Order')
plt.savefig('hc1_order.png', dpi=600)
plt.figure()
plt.plot(hc1_data[hc1_data['item'] == 'trust-ds2']['value'], color='orange', label='Trustworthiness of DS1')
plt.plot(hc1_data[hc1_data['item'] == 'trust-ds3']['value'], color='forestgreen', label='Trustworthiness of DS2')
plt.legend(loc='upper right')
plt.title('Healthcenter1 Trust')
plt.savefig('hc1_trust.png', dpi=600)

# Healthcenter 2
hc2_data = data[data['agent'] == 'hc_5']
hc2_data.set_index('time', inplace=True)
plt.figure()
plt.plot(hc2_data[hc2_data['item'] == 'inventory']['value'], color='navy', label='Inventory')
plt.plot(hc2_data[hc2_data['item'] == 'demand']['value'], color='orange', label='Demand')
plt.plot(hc2_data[hc2_data['item'] == 'backlog']['value'], color='forestgreen', label='Backlog')
plt.plot(hc2_data[hc2_data['item'] == 'on-order']['value'], color='firebrick', label='On-order')
plt.legend(loc='upper right')
plt.title('Healthcenter2 State')
plt.savefig('hc2_state.png', dpi=600)
plt.figure()
plt.plot(hc2_data[hc2_data['item'] == 'order-ds2']['value'], color='orange', label='Order to DS1')
plt.plot(hc2_data[hc2_data['item'] == 'order-ds3']['value'], color='forestgreen', label='Order to DS2')
plt.legend(loc='upper right')
plt.title('Healthcenter2 Order')
plt.savefig('hc2_order.png', dpi=600)
plt.figure()
plt.plot(hc2_data[hc2_data['item'] == 'trust-ds2']['value'], color='orange', label='Trustworthiness of DS1')
plt.plot(hc2_data[hc2_data['item'] == 'trust-ds3']['value'], color='forestgreen', label='Trustworthiness of DS2')
plt.legend(loc='upper right')
plt.title('Healthcenter2 Trust')
plt.savefig('hc2_trust.png', dpi=600)

# Distributor 1
ds1_data = data[data['agent'] == 'ds_2']
ds1_data.set_index('time', inplace=True)
plt.figure()
plt.plot(ds1_data[ds1_data['item'] == 'inventory']['value'], color='navy', label='Inventory')
plt.plot(ds1_data[ds1_data['item'] == 'demand']['value'], color='orange', label='Demand')
plt.plot(ds1_data[ds1_data['item'] == 'backlog']['value'], color='forestgreen', label='Backlog')
plt.plot(ds1_data[ds1_data['item'] == 'on-order']['value'], color='firebrick', label='On-order')
plt.legend(loc='upper right')
plt.title('Distributor 1')
plt.savefig('ds1.png', dpi=600)

# Distributor 2
ds2_data = data[data['agent'] == 'ds_3']
ds2_data.set_index('time', inplace=True)
plt.figure()
plt.plot(ds2_data[ds2_data['item'] == 'inventory']['value'], color='navy', label='Inventory')
plt.plot(ds2_data[ds2_data['item'] == 'demand']['value'], color='orange', label='Demand')
plt.plot(ds2_data[ds2_data['item'] == 'backlog']['value'], color='forestgreen', label='Backlog')
plt.plot(ds2_data[ds2_data['item'] == 'on-order']['value'], color='firebrick', label='On-order')
plt.legend(loc='upper right')
plt.title('Distributor 2')
plt.savefig('ds2.png', dpi=600)

# Manufacturer 1
mn1_data = data[data['agent'] == 'mn_0']
mn1_data.set_index('time', inplace=True)
plt.figure()
plt.plot(mn1_data[mn1_data['item'] == 'inventory']['value'], color='navy', label='Inventory')
plt.plot(mn1_data[mn1_data['item'] == 'demand']['value'], color='orange', label='Demand')
plt.plot(mn1_data[mn1_data['item'] == 'backlog']['value'], color='forestgreen', label='Backlog')
plt.plot(mn1_data[mn1_data['item'] == 'in_production']['value'], color='firebrick', label='In-production')
plt.legend(loc='upper right')
plt.title('Manufacturer 1')
plt.savefig('mn1.png', dpi=600)

# Manufacturer 2
mn2_data = data[data['agent'] == 'mn_1']
mn2_data.set_index('time', inplace=True)
plt.figure()
plt.plot(mn2_data[mn2_data['item'] == 'inventory']['value'], color='navy', label='Inventory')
plt.plot(mn2_data[mn2_data['item'] == 'demand']['value'], color='orange', label='Demand')
plt.plot(mn2_data[mn2_data['item'] == 'backlog']['value'], color='forestgreen', label='Backlog')
plt.plot(mn2_data[mn2_data['item'] == 'in_production']['value'], color='firebrick', label='In-production')
plt.legend(loc='upper right')
plt.title('Manufacturer 2')
plt.savefig('mn2.png', dpi=600)