from psychsim_agents.helper_functions import *
from simulator.distruption import LineShutDownDisruption
from study.simple_psychsim.simulation_profile_222 import *
from study.simple_psychsim import *
import simulator.network as network

ALLOCATE_PROPORTIONAL = 'alloc_prop'
ALLOCATE_EQUALLY = 'alloc_eq'
FALSE_MODEL = 'false_model'
TRUE_MODEL = 'true_model'


class PerfectActionHcDs(SimulationProfileBase222):
    """
    A simulation profile for testing the effect of distributor(s) having a false model of disrupted manufacturer's
    available allocation recipe.
    """

    def __init__(self, name, hc_planning_idxs, delta):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list hc_planning_idxs: a list with the indexes of the health centers with planning capacity.
        """
        self.hc_planning_idxs = hc_planning_idxs
        self.delta = delta
        super(PerfectActionHcDs, self).__init__(name)
        self.time_periods = 250

    def parameterize_agent_builder(self):
        """
        Sets the default parameters for the agent builder.
        """
        super(PerfectActionHcDs, self).parameterize_agent_builder()
        self.agent_builder.history_preserve_time = 40
        self.agent_builder.fixed_order_up_to_level = False


    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 70
        manufacturer_shutdown.end_day_1 = 80
        manufacturer_shutdown.decrease_factor_1 = 0.95
        self.simulation.disruptions.append(manufacturer_shutdown)

    def define_agent_connections(self):
        """
        Collects the references to the agents in the network. Does not change connectivity.
        """
        self.hc1 = self.simulation.health_centers[0]
        self.hc2 = self.simulation.health_centers[1]
        self.ds1 = self.simulation.distributors[0]
        self.ds2 = self.simulation.distributors[1]
        self.mn1 = self.simulation.manufacturers[0]
        self.mn2 = self.simulation.manufacturers[1]

        for ds in self.simulation.distributors:

            # ds <-> hc
            for hc in self.simulation.health_centers:
                hc.upstream_nodes.append(ds)
                ds.downstream_nodes.append(hc)
        self.simulation.distributors[0].upstream_nodes.append(self.simulation.manufacturers[0])
        self.simulation.distributors[1].upstream_nodes.append(self.simulation.manufacturers[1])
        self.simulation.manufacturers[0].downstream_nodes.append(self.simulation.distributors[0])
        self.simulation.manufacturers[1].downstream_nodes.append(self.simulation.distributors[1])

    def add_patient_model(self):
        """
        Adds one patient model to generate demand at the health centers.
        """
        hc1 = self.simulation.health_centers[0]
        hc2 = self.simulation.health_centers[1]
        patient_model = DifferentPatientModel([hc1, hc2])
        patient_model.urgent_1 = 0
        patient_model.non_urgent_1 = 120
        patient_model.urgent_2 = 0
        patient_model.non_urgent_2 = 120
        self.simulation.patient_model = patient_model

    def parameterize_psychsim_agents(self):
        super(PerfectActionHcDs, self).parameterize_psychsim_agents()

        # is horizon of 6 for allocation enough?
        for agent in self.agent_converter.all_psychsim_agents():
            agent.ps_agent.setHorizon(6)

    def add_reward_function(self):
    # set the reward function for manufacturers
        mn1 = self.agent_converter.get_psychsim_agent(self.mn1)
        mn1.tot_bklog_after_alloc_rwd_weight = 10.0
        mn1.inv_after_alloc_rwd_weight = 1.0
        mn1.allocate_rwd_weight = 5.0

        mn1.ps_agent.setReward(minimizeFeature(mn1.inv_after_allocation), mn1.inv_after_alloc_rwd_weight)
        mn1.ps_agent.setReward(
            minimizeFeature(mn1.total_backlog_after_allocation), mn1.tot_bklog_after_alloc_rwd_weight)
        for d in mn1.down_stream_agents:
            mn1.ps_agent.setReward(maximizeFeature(mn1.allocate_to[d]), mn1.allocate_rwd_weight)

        mn2 = self.agent_converter.get_psychsim_agent(self.mn2)
        mn2.tot_bklog_after_alloc_rwd_weight = 10.0
        mn2.inv_after_alloc_rwd_weight = 1.0
        mn2.allocate_rwd_weight = 5.0

        mn2.ps_agent.setReward(minimizeFeature(mn2.inv_after_allocation), mn2.inv_after_alloc_rwd_weight)
        mn2.ps_agent.setReward(
            minimizeFeature(mn2.total_backlog_after_allocation), mn2.tot_bklog_after_alloc_rwd_weight)
        for d in mn2.down_stream_agents:
            mn2.ps_agent.setReward(maximizeFeature(mn2.allocate_to[d]), mn2.allocate_rwd_weight)

        # set the reward function of the distributors
        ds1 = self.agent_converter.get_psychsim_agent(self.ds1)
        ds1.tot_bklog_after_alloc_rwd_weight = 10.0
        ds1.inv_after_alloc_rwd_weight = 1.0
        ds1.allocate_rwd_weight = 5.0
        ds1.ps_agent.setReward(minimizeFeature(ds1.inv_after_allocation), ds1.inv_after_alloc_rwd_weight)
        ds1.ps_agent.setReward(
            minimizeFeature(ds1.total_backlog_after_allocation), ds1.tot_bklog_after_alloc_rwd_weight)
        for d in ds1.down_stream_agents:
            ds1.ps_agent.setReward(maximizeFeature(ds1.allocate_to[d]), ds1.allocate_rwd_weight)

        # ds2
        ds2 = self.agent_converter.get_psychsim_agent(self.ds2)
        ds2.tot_bklog_after_alloc_rwd_weight = 10.0
        ds2.inv_after_alloc_rwd_weight = 1.0
        ds2.allocate_rwd_weight = 5.0
        ds2.ps_agent.setReward(minimizeFeature(ds2.inv_after_allocation), ds2.inv_after_alloc_rwd_weight)
        ds2.ps_agent.setReward(
            minimizeFeature(ds2.total_backlog_after_allocation), ds2.tot_bklog_after_alloc_rwd_weight)
        for d in ds2.down_stream_agents:
            ds2.ps_agent.setReward(maximizeFeature(ds2.allocate_to[d]), ds2.allocate_rwd_weight)

        # set reward for health-centers
        hc1 = self.agent_converter.get_psychsim_agent(self.hc1)
        hc1.inv_rwd_weight = 1.0
        hc1.lost_urgent_rwd_weight = 100.0
        hc1.lost_non_urgent_rwd_weight = 10.0

        hc1.ps_agent.setReward(minimizeFeature(hc1.inv_after_allocation), hc1.inv_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.lost_urgent), hc1.lost_urgent_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.backlog), hc1.lost_non_urgent_rwd_weight)

        hc2 = self.agent_converter.get_psychsim_agent(self.hc2)
        hc2.inv_rwd_weight = 1.0
        hc2.lost_urgent_rwd_weight = 100.0
        hc2.lost_non_urgent_rwd_weight = 10.0

        hc2.ps_agent.setReward(minimizeFeature(hc2.inv_after_allocation), hc2.inv_rwd_weight)
        hc2.ps_agent.setReward(minimizeFeature(hc2.lost_urgent), hc2.lost_urgent_rwd_weight)
        hc2.ps_agent.setReward(minimizeFeature(hc2.backlog), hc2.lost_non_urgent_rwd_weight)

    def add_health_centers_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim health centers.
        """
        order_offset = 0.2
        order_up_to = HospitalUpToOrderAmountRecipe('1ord_up')
        order_more = HospitalUpToOrderAmountRecipe('ord+', order_offset)
        order_less = HospitalUpToOrderAmountRecipe('ord-', -order_offset)
        order_amt_recipes_planning = [order_up_to, order_less, order_more]
        order_amt_recipes_no_planning = [order_up_to]

        # gets health centers with planning capacity
        planning_hc = []
        for idx in self.hc_planning_idxs:
            planning_hc.append(self.simulation.health_centers[idx])

        # adds recipes
        for hc in self.simulation.health_centers:
            order_amt_recipes = order_amt_recipes_no_planning
            if hc in planning_hc:
                order_amt_recipes = order_amt_recipes_planning

            available_recipes = {
                ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: order_amt_recipes,
                ORDERING_SPLIT: [HospitalOrderSplitEquallyRecipe('split_eq')],
                TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', self.delta)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(hc)
            agent.define_state_dynamics(available_recipes)

    def add_distributors_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim distributors.
        Default is allocate proportional, order up-to, order split by trust, update trust by history.
        """
        for ds in self.simulation.distributors:
            # creates list of recipes
            available_recipes = {
                # ALLOCATION: [DistributorAllocateProportionalRecipe('alloc_prop')],
                ALLOCATION: [DistributorAllocateEquallyRecipe('alloc_eq')],
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: [DistributorUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_eq')],
                # TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)


    def parameterize_sim_agents(self):
        """
        sets up-to level for 2X2X2 complete network
        """
        # default parametrization
        super(PerfectActionHcDs, self).parameterize_sim_agents()
        now = self.simulation.now
        for agent in self.simulation.agents:
            agent.up_to_level = agent.order_up_to_level_calculator.calculate(now)

        mn1 = self.simulation.manufacturers[0]
        mn2 = self.simulation.manufacturers[1]
        ds1 = self.simulation.distributors[0]
        ds2 = self.simulation.distributors[1]
        hc1 = self.simulation.health_centers[0]
        hc2 = self.simulation.health_centers[1]

        # initialize to stable state
        for up_agent in hc1.upstream_nodes:
            hc1.ontime_deliv_rate[up_agent] = 1
            # hc1.trust[up_agent] = 1
            hc1.expctd_on_order[up_agent] = [60, 60]

        for up_agent in hc2.upstream_nodes:
            hc2.ontime_deliv_rate[up_agent] = 1
            # hc2.trust[up_agent] = 1
            hc2.expctd_on_order[up_agent] = [60, 60]
        # HC1 now orders from DS1
        order = Order()
        order.src = hc1
        order.dst = ds1
        order.amount = 60
        hc1.on_order.append(order)
        message = network.OrderMessage(copy.copy(order))
        message.leadTime = 0
        message.src = order.src
        message.dst = order.dst
        self.simulation.info_network.payloads.append(message)
        hist = ds1.get_history_item(0)
        hist['incoming_order'].append(copy.copy(order))

        # # HC1 previous order from DS1
        order = Order()
        order.src = hc1
        order.dst = ds1
        order.amount = 60
        hc1.on_order.append(order)

        # # DS1 has started sending products to HC1
        item = Item()
        item.amount = 60
        in_transit = InTransit(item)
        in_transit.src = ds1
        in_transit.dst = hc1
        in_transit.leadTime = 1
        self.simulation.network.payloads.append(in_transit)

        # # HC1 now orders from DS2
        order = Order()
        order.src = hc1
        order.dst = ds2
        order.amount = 60
        hc1.on_order.append(order)
        message = network.OrderMessage(copy.copy(order))
        message.leadTime = 0
        message.src = order.src
        message.dst = order.dst
        self.simulation.info_network.payloads.append(message)
        hist = ds2.get_history_item(0)
        hist['incoming_order'].append(copy.copy(order))
        # # HC1 previous order from DS2
        order = Order()
        order.src = hc1
        order.dst = ds2
        order.amount = 60
        hc1.on_order.append(order)

        # # DS2 has started sending products to HC1
        item = Item()
        item.amount = 60
        in_transit = InTransit(item)
        in_transit.src = ds2
        in_transit.dst = hc1
        in_transit.leadTime = 1
        self.simulation.network.payloads.append(in_transit)

        # # HC2 now orders from DS1
        order = Order()
        order.src = hc2
        order.dst = ds1
        order.amount = 60
        hc2.on_order.append(order)
        message = network.OrderMessage(copy.copy(order))
        message.leadTime = 0
        message.src = order.src
        message.dst = order.dst
        self.simulation.info_network.payloads.append(message)
        hist = ds1.get_history_item(0)
        hist['incoming_order'].append(copy.copy(order))
        # # HC2 previous order from DS1
        order = Order()
        order.src = hc2
        order.dst = ds1
        order.amount = 60
        hc2.on_order.append(order)

        # # DS1 has started sending products to HC2
        item = Item()
        item.amount = 60
        in_transit = InTransit(item)
        in_transit.src = ds1
        in_transit.dst = hc2
        in_transit.leadTime = 1
        self.simulation.network.payloads.append(in_transit)

        # # HC2 now orders from DS2
        order = Order()
        order.src = hc2
        order.dst = ds2
        order.amount = 60
        hc2.on_order.append(order)
        message = network.OrderMessage(copy.copy(order))
        message.leadTime = 0
        message.src = order.src
        message.dst = order.dst
        self.simulation.info_network.payloads.append(message)
        hist = ds2.get_history_item(0)
        hist['incoming_order'].append(copy.copy(order))
        # # HC2 previous order from DS2
        order = Order()
        order.src = hc2
        order.dst = ds2
        order.amount = 60
        hc2.on_order.append(order)

        # # DS2 has started sending products to HC2
        item = Item()
        item.amount = 60
        in_transit = InTransit(item)
        in_transit.src = ds2
        in_transit.dst = hc2
        in_transit.leadTime = 1
        self.simulation.network.payloads.append(in_transit)

        # # DS1 now orders from MN1
        order = Order()
        order.src = ds1
        order.dst = mn1
        order.amount = 120
        ds1.on_order.append(order)
        message = network.OrderMessage(copy.copy(order))
        message.leadTime = 0
        message.src = order.src
        message.dst = order.dst
        self.simulation.info_network.payloads.append(message)
        hist = mn1.get_history_item(0)
        hist['incoming_order'].append(copy.copy(order))
        # # DS1 previous order from MN1
        order = Order()
        order.src = ds1
        order.dst = mn1
        order.amount = 120
        ds1.on_order.append(order)
        # MN1 has started sending products to DS1
        item = Item()
        item.amount = 120
        in_transit = InTransit(item)
        in_transit.src = mn1
        in_transit.dst = ds1
        in_transit.leadTime = 1
        self.simulation.network.payloads.append(in_transit)

        # # DS1 now orders from MN1
        order = Order()
        order.src = ds2
        order.dst = mn2
        order.amount = 120
        ds2.on_order.append(order)
        message = network.OrderMessage(copy.copy(order))
        message.leadTime = 0
        message.src = order.src
        message.dst = order.dst
        self.simulation.info_network.payloads.append(message)
        hist = mn2.get_history_item(0)
        hist['incoming_order'].append(copy.copy(order))
        # # DS1 previous order from MN1
        order = Order()
        order.src = ds2
        order.dst = mn2
        order.amount = 120
        ds2.on_order.append(order)
        # MN1 has started sending products to DS1
        item = Item()
        item.amount = 120
        in_transit = InTransit(item)
        in_transit.src = mn2
        in_transit.dst = ds2
        in_transit.leadTime = 1
        self.simulation.network.payloads.append(in_transit)

        item = Item()
        item.amount = 120
        item.lead_time = 2
        mn1.in_production.append(item)

        item = Item()
        item.amount = 120
        item.lead_time = 1
        mn1.in_production.append(item)

        item = Item()
        item.amount = 120
        item.lead_time = 2
        mn2.in_production.append(item)

        item = Item()
        item.amount = 120
        item.lead_time = 1
        mn2.in_production.append(item)

        hist = hc1.get_history_item(0)
        hist['patient'] = (0, 120)

        hist = hc2.get_history_item(0)
        hist['patient'] = (0, 120)