from study.simple_psychsim.simulation_profile import *


class SimulationProfileBase111(SimulationProfileBase):
    """
    A simulation base profile for 2x2x2 experiments containing 2 manufacturers, 2 distributors and 2 health centers.
    """

    def __init__(self, name):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        """
        # todo Scenarios:
        # - imperfect perception (health center has false model of manufacturer: disruption vs full capacity)
        # - imperfect actions (distributor has false model of manufacturer: AllocateEqually vs AllocateProportional)
        # - trust impact (distributor splitOrdEqually vs ByTrust)
        # - distributor lookahead vs no-lookahead (perfect models) (different network structures)
        # - health center allocate urgent-first vs proportional
        self.hc1 = None
        self.ds1 = None
        self.mn1 = None
        super(SimulationProfileBase111, self).__init__(name, 1, 1, 1)

    def define_agent_connections(self):
        """
        Collects the references to the agents in the network. Does not change connectivity.
        """
        super(SimulationProfileBase111, self).define_agent_connections()

        self.hc1 = self.simulation.health_centers[0]
        self.ds1 = self.simulation.distributors[0]
        self.mn1 = self.simulation.manufacturers[0]

    def parameterize_sim_agents(self):
        """
        sets up-to level for 2X2X2 complete network
        """

        # default parametrization
        super(SimulationProfileBase111, self).parameterize_sim_agents()

        for agent in self.simulation.agents:
            agent.up_to_level = 240
