from simulator.distruption import LineShutDownDisruption
from study.simple_psychsim import *


class TrustOrderSplitRecipeNet2(SimulationProfileBase222):
    """
    A simulation profile for testing the effect of distributors & health_centers splitting orders to upstreams based on
    trust or equally.
    """

    def __init__(self, name, dist_planning_idxs, hc_planning_idxs, trust_weight):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list dist_planning_idxs: a list with the indexes of the distributors with planning capacity.
        :param list hc_planning_idxs: a list with the indexes of the health centers with planning capacity.
        :param float trust_weight: the weight associated with the old value when updating trust.
        """
        self.dist_planning_idxs = dist_planning_idxs
        self.hc_planning_idxs = hc_planning_idxs
        self.trust_weight = trust_weight
        super(TrustOrderSplitRecipeNet2, self).__init__(name)

    def define_agent_connections(self):
        # sets default full connectivity
        super(TrustOrderSplitRecipeNet2, self).define_agent_connections()

        # DS2 is connected only to MN2 and hence MN1 is only connected to DS1.
        self.ds2.upstream_nodes = [self.mn2]
        self.mn1.downstream_nodes = [self.ds1]

    def parameterize_sim_agents(self):
        # default parametrization up-to level for 2X2X2 complete network
        super(TrustOrderSplitRecipeNet2, self).parameterize_sim_agents()

        # different up-to-levels for manufacturers
        self.mn1.up_to_level = 120
        self.mn2.up_to_level = 360

    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 20
        manufacturer_shutdown.end_day_1 = 30
        manufacturer_shutdown.decrease_factor_1 = 0.625
        self.simulation.disruptions.append(manufacturer_shutdown)

    def add_health_centers_recipes(self):

        # sets up ordering recipes
        split_by_trust = HospitalOrderSplitByTrustRecipe('split_trust')
        split_equally = HospitalOrderSplitEquallyRecipe('split_eq')
        split_ordr_recipes_planning = [split_by_trust, split_equally]
        split_ordr_recipes_no_planning = [split_equally]

        # gets distributors and health centers with planning capacity
        planning_hc = []
        for idx in self.hc_planning_idxs:
            planning_hc.append(self.simulation.health_centers[idx])

        # adds corresponding recipes according to distributors's planning capacity
        for hc in self.simulation.health_centers:

            split_ordr_recipes = split_ordr_recipes_no_planning
            if hc in planning_hc:
                split_ordr_recipes = split_ordr_recipes_planning

            available_recipes = {
                ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: [HospitalUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: split_ordr_recipes,
                TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', self.trust_weight)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(hc)
            agent.define_state_dynamics(available_recipes)

    def add_distributors_recipes(self):

        # sets up ordering recipes
        split_by_trust = DistributorOrderSplitByTrustRecipe('split_trust')
        split_equally = DistributorOrderSplitEquallyRecipe('split_eq')
        split_ordr_recipes_planning = [split_by_trust, split_equally]
        split_ordr_recipes_no_planning = [split_equally]

        # gets distributors and health centers with planning capacity
        planning_ds = []
        for idx in self.dist_planning_idxs:
            planning_ds.append(self.simulation.distributors[idx])

        # adds corresponding recipes according to distributors's planning capacity
        for ds in self.simulation.distributors:

            split_ordr_recipes = split_ordr_recipes_no_planning
            if ds in planning_ds:
                split_ordr_recipes = split_ordr_recipes_planning

            available_recipes = {
                ALLOCATION: [DistributorAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: [DistributorUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: split_ordr_recipes,
                TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', self.trust_weight)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)
