from simulator.distruption import LineShutDownDisruption
from study.simple_psychsim import *


class PerfectDistLookaheadNet2DiffReward(SimulationProfileBase222):
    """
    A simulation profile for testing the effect of distributors performing forward planning vs. not performing
    forward planning.
    """

    def __init__(self, name, dist_lookahead_idxs):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list dist_lookahead_idxs: a list with the indexes of the distributors with planning capacity.
        """
        self.dist_lookahead_idxs = dist_lookahead_idxs
        super(PerfectDistLookaheadNet2DiffReward, self).__init__(name)

    # def define_agent_connections(self):
    #     # sets default full connectivity
    #     super(PerfectDistLookaheadNet2DiffReward, self).define_agent_connections()
    #
    #     # DS2 is connected only to MN2 and hence MN1 is only connected to DS1.
    #     self.ds2.upstream_nodes = [self.mn2]
    #     self.mn1.downstream_nodes = [self.ds1]
    #
    # def parameterize_sim_agents(self):
    #     # default parametrization up-to level for 2X2X2 complete network
    #     super(PerfectDistLookaheadNet2DiffReward, self).parameterize_sim_agents()
    #
    #     # different up-to-levels for manufacturers
    #     self.mn1.up_to_level = 120
    #     self.mn2.up_to_level = 360

    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 20
        manufacturer_shutdown.end_day_1 = 30
        manufacturer_shutdown.decrease_factor_1 = 0.75
        self.simulation.disruptions.append(manufacturer_shutdown)

    def parameterize_psychsim_agents(self):
        """
        Parametrizes all psychsim agents.
        Default is all agents have a 6 period look-ahead with a discount of 1. Sets default values for other agents.
        """

        # default parameters for all agents
        for agent in self.agent_converter.all_psychsim_agents():
            agent.ps_agent.setHorizon(6)  # Horizon is how far ahead agent reasons to determine next action
            agent.ps_agent.setRecursiveLevel(1)
            agent.ps_agent.setAttribute('discount', 1.0)  # Discount is how much agent discounts future rewards
            # agent.ps_agent.setAttribute('discretization', 10)  # sets the discretization level / number of groups
            agent.smoothing_constant = 0.8

        # default parameters for health centers
        for hc in self.simulation.health_centers:
            agent = self.agent_converter.get_psychsim_agent(hc)
            agent.totals = {INV_HI: 400, UP_TO_HI: 400, UP_STR_HI: 3, ORDER_HI: 400, DEM_HI: 400, CUR_DEM_HI: 400,
                            LEAD_TIME_HI: 4}
            agent.inv_rwd_weight = 1.0
            agent.lost_urgent_rwd_weight = 100.0
            agent.lost_non_urgent_rwd_weight = 100.0

        # default parameters for manufacturers
        for mn in self.simulation.manufacturers:
            agent = self.agent_converter.get_psychsim_agent(mn)
            agent.totals = {INV_HI: 400, UP_TO_HI: 400, DN_STR_HI: 3, ORDER_HI: 400, DEM_HI: 400, PROD_HI: 400,
                            LINE_HI: 10, BL_HI: 400, CUR_DEM_HI: 400, LEAD_TIME_HI: 3}
            agent.tot_bklog_after_alloc_rwd_weight = 10.0
            agent.inv_after_alloc_rwd_weight = 1.0
            agent.allocate_rwd_weight = 5.0

        # default parameters for distributors
        for ds in self.simulation.distributors:
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.totals = {INV_HI: 400, UP_TO_HI: 400, UP_STR_HI: 3, DN_STR_HI: 3, ORDER_HI: 400, DEM_HI: 400,
                            BL_HI: 400, CUR_DEM_HI: 400, LEAD_TIME_HI: 3}
            if ds.id == 2:
                agent.tot_bklog_after_alloc_rwd_weight = 1000.0
                agent.inv_after_alloc_rwd_weight = 1.0
                agent.allocate_rwd_weight = 1.0
            else:
                agent.tot_bklog_after_alloc_rwd_weight = 0.0
                agent.inv_after_alloc_rwd_weight = 0.0
                agent.allocate_rwd_weight = 0.0

        # default parameters for cleaning agent
        self.cleaning_agent.totals = {ORDER_HI: 1000}

    def add_distributors_recipes(self):

        # sets up ordering recipes
        order_offset = 0.2
        order_up_to = DistributorUpToOrderAmountRecipe('1ord_up')
        order_more = DistributorUpToOrderAmountRecipe('ord+', order_offset)
        order_less = DistributorUpToOrderAmountRecipe('ord-', -order_offset)
        order_amt_recipes_planning = [order_up_to, order_less, order_more]
        order_amt_recipes_no_planning = [order_up_to]

        # gets distributors with planning capacity
        planning_ds = []
        for idx in self.dist_lookahead_idxs:
            planning_ds.append(self.simulation.distributors[idx])

        # adds corresponding recipes
        for ds in self.simulation.distributors:

            order_amt_recipes = order_amt_recipes_no_planning
            if ds in planning_ds:
                order_amt_recipes = order_amt_recipes_planning

            available_recipes = {
                ALLOCATION: [DistributorAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: order_amt_recipes,
                ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_eq')],
                TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)
