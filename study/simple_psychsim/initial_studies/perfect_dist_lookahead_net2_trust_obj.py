from simulator.distruption import LineShutDownDisruption
from study.simple_psychsim import *


class PerfectDistLookaheadNet2TrustObj(SimulationProfileBase222):
    """
    A simulation profile for testing the effect of distributors performing forward planning vs. not performing
    forward planning.
    """

    def __init__(self, name, dist_lookahead_idxs):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list dist_lookahead_idxs: a list with the indexes of the distributors with planning capacity.
        """
        self.dist_lookahead_idxs = dist_lookahead_idxs
        super(PerfectDistLookaheadNet2TrustObj, self).__init__(name)
        self.time_periods = 60

    def parameterize_agent_builder(self):
        """
        Sets the default parameters for the agent builder.
        """
        super(PerfectDistLookaheadNet2TrustObj, self).parameterize_agent_builder()
        self.agent_builder.history_preserve_time = 10

    #
    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 20
        manufacturer_shutdown.end_day_1 = 30
        manufacturer_shutdown.decrease_factor_1 = 0.85
        self.simulation.disruptions.append(manufacturer_shutdown)

    def add_patient_model(self):
        """
        Adds one patient model to generate demand at the health centers.
        Default is a constant patient model for all health centers in the network.
        """
        patient_model = ConstantPatientModel(self.simulation.health_centers)
        self.simulation.patient_model = patient_model
        patient_model.urgent = 120
        patient_model.non_urgent = 0

    def add_reward_function(self):
    # set the reward function for manufacturers
        mn1 = self.agent_converter.get_psychsim_agent(self.mn1)
        mn1.tot_bklog_after_alloc_rwd_weight = 10.0
        mn1.inv_after_alloc_rwd_weight = 1.0
        mn1.allocate_rwd_weight = 5.0

        mn1.ps_agent.setReward(minimizeFeature(mn1.inv_after_allocation), mn1.inv_after_alloc_rwd_weight)
        mn1.ps_agent.setReward(
            minimizeFeature(mn1.total_backlog_after_allocation), mn1.tot_bklog_after_alloc_rwd_weight)
        for d in mn1.down_stream_agents:
            mn1.ps_agent.setReward(maximizeFeature(mn1.allocate_to[d]), mn1.allocate_rwd_weight)

        mn2 = self.agent_converter.get_psychsim_agent(self.mn2)
        mn2.tot_bklog_after_alloc_rwd_weight = 10.0
        mn2.inv_after_alloc_rwd_weight = 1.0
        mn2.allocate_rwd_weight = 5.0

        mn2.ps_agent.setReward(minimizeFeature(mn2.inv_after_allocation), mn2.inv_after_alloc_rwd_weight)
        mn2.ps_agent.setReward(
            minimizeFeature(mn2.total_backlog_after_allocation), mn2.tot_bklog_after_alloc_rwd_weight)
        for d in mn2.down_stream_agents:
            mn2.ps_agent.setReward(maximizeFeature(mn2.allocate_to[d]), mn2.allocate_rwd_weight)

        # set the reward function of the distributors
        ds1 = self.agent_converter.get_psychsim_agent(self.ds1)
        ds1.trust_weight = 1.0
        # ds1.allocate_rwd_weight = 1.0

        for d in ds1.down_stream_agents:
            ds1.ps_agent.setReward(minimizeFeature(d.trust[ds1]), ds1.trust_weight)
            # ds1.ps_agent.setReward(maximizeFeature(ds1.allocate_to[d]), ds1.allocate_rwd_weight)
        # ds1 = self.agent_converter.get_psychsim_agent(self.ds1)
        # ds1.tot_bklog_after_alloc_rwd_weight = 10.0
        # ds1.inv_after_alloc_rwd_weight = 1.0
        # ds1.allocate_rwd_weight = 5.0
        #
        # ds1.ps_agent.setReward(minimizeFeature(ds1.inv_after_allocation), ds1.inv_after_alloc_rwd_weight)
        # ds1.ps_agent.setReward(
        #     minimizeFeature(ds1.total_backlog_after_allocation), ds1.tot_bklog_after_alloc_rwd_weight)
        # for d in ds1.down_stream_agents:
        #     ds1.ps_agent.setReward(maximizeFeature(ds1.allocate_to[d]), ds1.allocate_rwd_weight)
        # ds2
        ds2 = self.agent_converter.get_psychsim_agent(self.ds2)
        ds2.tot_bklog_after_alloc_rwd_weight = 10.0
        ds2.inv_after_alloc_rwd_weight = 1.0
        ds2.allocate_rwd_weight = 5.0

        ds2.ps_agent.setReward(minimizeFeature(ds2.inv_after_allocation), ds2.inv_after_alloc_rwd_weight)
        ds2.ps_agent.setReward(
            minimizeFeature(ds2.total_backlog_after_allocation), ds2.tot_bklog_after_alloc_rwd_weight)
        for d in ds2.down_stream_agents:
            ds2.ps_agent.setReward(maximizeFeature(ds2.allocate_to[d]), ds2.allocate_rwd_weight)
        # set reward for health-centers
        hc1 = self.agent_converter.get_psychsim_agent(self.hc1)
        hc1.inv_rwd_weight = 1.0
        hc1.lost_urgent_rwd_weight = 100.0
        hc1.lost_non_urgent_rwd_weight = 10.0

        hc1.ps_agent.setReward(minimizeFeature(hc1.inv_after_allocation), hc1.inv_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.lost_urgent), hc1.lost_urgent_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.backlog), hc1.lost_non_urgent_rwd_weight)

        hc2 = self.agent_converter.get_psychsim_agent(self.hc2)
        hc2.inv_rwd_weight = 1.0
        hc2.lost_urgent_rwd_weight = 100.0
        hc2.lost_non_urgent_rwd_weight = 10.0

        hc2.ps_agent.setReward(minimizeFeature(hc2.inv_after_allocation), hc2.inv_rwd_weight)
        hc2.ps_agent.setReward(minimizeFeature(hc2.lost_urgent), hc2.lost_urgent_rwd_weight)
        hc2.ps_agent.setReward(minimizeFeature(hc2.backlog), hc2.lost_non_urgent_rwd_weight)

    def parameterize_psychsim_agents(self):
        super(PerfectDistLookaheadNet2TrustObj, self).parameterize_psychsim_agents()

        # default parameters for all agents
        for agent in self.agent_converter.all_psychsim_agents():
            agent.ps_agent.setHorizon(12)  # Horizon is how far ahead agent reasons to determine next action

    def add_health_centers_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim health centers.
        Default is allocate proportional, order up-to, order split by trust, update trust by history.
        """
        for hc in self.simulation.health_centers:
            # creates list of recipes

            available_recipes = {
                ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: [HospitalUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: [HospitalOrderSplitByTrustRecipe('split_trust')],
                TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }

            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(hc)
            agent.define_state_dynamics(available_recipes)

    def add_distributors_recipes(self):

        # sets up ordering recipes
        order_offset = 0.2
        order_up_to = DistributorUpToOrderAmountRecipe('1ord_up')
        order_more = DistributorUpToOrderAmountRecipe('ord+', order_offset)
        order_less = DistributorUpToOrderAmountRecipe('ord-', -order_offset)
        order_amt_recipes_planning = [order_up_to, order_less, order_more]
        order_amt_recipes_no_planning = [order_up_to]

        # gets distributors with planning capacity
        planning_ds = []
        for idx in self.dist_lookahead_idxs:
            planning_ds.append(self.simulation.distributors[idx])

        # adds corresponding recipes
        for ds in self.simulation.distributors:

            order_amt_recipes = order_amt_recipes_no_planning
            if ds in planning_ds:
                order_amt_recipes = order_amt_recipes_planning

            available_recipes = {
                ALLOCATION: [DistributorAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: order_amt_recipes,
                ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_equal')],
                TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)
