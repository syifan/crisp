GOAL:
- testing the effect of weighting scheme of objective function on distributors performing forward planning vs. not performing forward planning

NETWORK STRUCTURE
- nodes:
    2 manufacturers (mn1 and mn2)
    2 distributors (ds1 and ds2)
    2 health centers (hc1 and hc2)
- network connections:
    (mn1, ds1)
    (mn2, ds1)
    (mn2, ds2)
    (ds1, hc1)
    (ds1, hc2)
    (ds2, hc1)
    (ds2, hc2)

DEMAND PROFILE
- each hc has 100 non-urgent, 20 urgent patients

ACTIONS
- distributors with planning have order up-to, order more and order less

DISRUPTION
- mn1 has no disruption
- mn2 has disruption between 20 and 30, its capacity reduces to 80% of the original capacity

PERCEPTION
- all agents have perfect model of each other

PLANNING
- ds1, ds2, or both have forward planning / lookahead capacity

AGENTS REWARD WEIGHTS
- different weight for each distributor
- same weight for both health centers
- same weight for both manufacturers
