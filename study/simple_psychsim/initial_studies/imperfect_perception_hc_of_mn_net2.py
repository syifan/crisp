from simulator.distruption import LineShutDownDisruption
from study.simple_psychsim import *
from study.simple_psychsim import SimulationProfileBase222


class ImperfectPerceptionHcMnNet2(SimulationProfileBase222):
    """
    A simulation profile for testing the effect of health center(s) having false perception about disrupted
    manufacturer's available capacity.
    """

    def __init__(self, name, health_center_idxs, hc_planning_idxs):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list health_center_idxs: a list with the indexes of the health centers having the false perception over
        the manufacturer's capacity.
        :param list hc_planning_idxs: a list with the indexes of the distributors with planning capacity.
        """
        self.health_center_idxs = health_center_idxs
        self.hc_planning_idxs = hc_planning_idxs
        super(ImperfectPerceptionHcMnNet2, self).__init__(name)

    def define_agent_connections(self):
        # sets default full connectivity
        super(ImperfectPerceptionHcMnNet2, self).define_agent_connections()

        # DS2 is connected only to MN2 and hence MN1 is only connected to DS1.
        self.ds2.upstream_nodes = [self.mn2]
        self.mn1.downstream_nodes = [self.ds1]

    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 20
        manufacturer_shutdown.end_day_1 = 30
        manufacturer_shutdown.decrease_factor_1 = 0.625
        self.simulation.disruptions.append(manufacturer_shutdown)

    def add_health_centers_recipes(self):
        # options for health centers: ordering recipe
        order_offset = 0.2
        order_up_to = HospitalUpToOrderAmountRecipe('1ord_up')
        order_more = HospitalUpToOrderAmountRecipe('ord+', order_offset)
        order_less = HospitalUpToOrderAmountRecipe('ord-', -order_offset)
        order_amt_recipes_planning = [order_up_to, order_less, order_more]
        order_amt_recipes_no_planning = [order_up_to]

        # gets health center with planning capacity
        planning_hc = []
        for idx in self.hc_planning_idxs:
            planning_hc.append(self.simulation.health_centers[idx])

        for hc in self.simulation.health_centers:
            order_amt_recipes = order_amt_recipes_no_planning
            if hc in planning_hc:
                order_amt_recipes = order_amt_recipes_planning
            available_recipes = {
                ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: order_amt_recipes,
                ORDERING_SPLIT: [HospitalOrderSplitEquallyRecipe('split_eq')],
                # TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', 0.2)]
            }

            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(hc)
            agent.define_state_dynamics(available_recipes)

    def add_mental_models(self):

        # adds a mental copy of the 'True' model for each agent and sets that model for all other agents.
        super(ImperfectPerceptionHcMnNet2, self).add_mental_models()

        # tells HC1's model (used by others) to ignore the parent's beliefs (imperfections)
        hc1 = self.agent_converter.get_psychsim_agent(self.hc1).ps_agent
        hc1.setAttribute('ignore-parent-beliefs', True, TRUE_MODEL)

    def add_perceptual_beliefs(self):
        mn2 = self.agent_converter.get_psychsim_agent(self.mn2)

        # set health center(s) beliefs about MN2 (capacity always full)
        for idx in self.health_center_idxs:
            hc = self.agent_converter.get_psychsim_agent(self.simulation.health_centers[idx]).ps_agent
            hc.setBelief(mn2.available_capacity, 400)
            hc.setBelief(mn2.total_inventory, 181)
            hc.setBelief(mn2.total_in_production, 181)
            hc.setBelief(mn2.production_dock, 0)
