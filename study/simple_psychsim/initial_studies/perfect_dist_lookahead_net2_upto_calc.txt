GOAL:
- testing the effect of having up to level calculator for constant demand-ds planning vs not planning

NETWORK STRUCTURE
- nodes:
    2 manufacturers (mn1 and mn2)
    2 distributors (ds1 and ds2)
    2 health centers (hc1 and hc2)
- network connections:
    (mn1, ds1)
    (mn2, ds1)
    (mn2, ds2)
    (ds1, hc1)
    (ds1, hc2)
    (ds2, hc1)
    (ds2, hc2)

DEMAND PROFILE
- each hc has 120 non-urgent and urgent patients
- up-to level is calculated dynamically

ACTIONS
- distributors with planning have order up-to, order more and order less

DISRUPTION
- mn1 has no disruption
- mn2 has disruption between 20 and 30, its capacity reduces to 85% of the original capacity

PERCEPTION
- all agents have perfect model of each other

PLANNING
- ds1, ds2, or both have forward planning / lookahead capacity