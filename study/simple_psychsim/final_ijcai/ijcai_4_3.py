from simulator.distruption import LineShutDownDisruption
from study.simple_psychsim.simulation_profile_222 import *

class Ijcai_4_3(SimulationProfileBase222):
    """
    A simulation profile for testing having two health-centers with one ordering based on trust and
     one ordering equally, testing if distributors will treat them differently.
    """

    def __init__(self, name, dist_lookahead_idxs):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list dist_lookahead_idxs: a list with the indexes of the distributors with planning capacity.
        """
        self.dist_lookahead_idxs = dist_lookahead_idxs
        super(Ijcai_4_3, self).__init__(name)
        self.time_periods = 250

    def parameterize_agent_builder(self):
        """
        Sets the default parameters for the agent builder.
        """
        super(Ijcai_4_3, self).parameterize_agent_builder()
        self.agent_builder.history_preserve_time = 40
        self.agent_builder.fixed_order_up_to_level = False


    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 70
        manufacturer_shutdown.end_day_1 = 80
        manufacturer_shutdown.decrease_factor_1 = 0.825

        manufacturer_shutdown.happen_day_2 = 90
        manufacturer_shutdown.end_day_2 = 100
        manufacturer_shutdown.decrease_factor_2 = 0.825
        self.simulation.disruptions.append(manufacturer_shutdown)

    def define_agent_connections(self):
        """
        Collects the references to the agents in the network. Does not change connectivity.
        """
        self.hc1 = self.simulation.health_centers[0]
        self.hc2 = self.simulation.health_centers[1]
        self.ds1 = self.simulation.distributors[0]
        self.ds2 = self.simulation.distributors[1]
        self.mn1 = self.simulation.manufacturers[0]
        self.mn2 = self.simulation.manufacturers[1]

        for ds in self.simulation.distributors:

            # ds <-> hc
            for hc in self.simulation.health_centers:
                hc.upstream_nodes.append(ds)
                ds.downstream_nodes.append(hc)
        self.simulation.distributors[0].upstream_nodes.append(self.simulation.manufacturers[0])
        self.simulation.distributors[1].upstream_nodes.append(self.simulation.manufacturers[1])
        self.simulation.manufacturers[0].downstream_nodes.append(self.simulation.distributors[0])
        self.simulation.manufacturers[1].downstream_nodes.append(self.simulation.distributors[1])

    def add_patient_model(self):
        """
        Adds one patient model to generate demand at the health centers.
        """
        hc1 = self.simulation.health_centers[0]
        hc2 = self.simulation.health_centers[1]
        patient_model = DifferentPatientModel([hc1, hc2])
        patient_model.urgent_1 = 0
        patient_model.non_urgent_1 = 120
        patient_model.urgent_2 = 0
        patient_model.non_urgent_2 = 120
        self.simulation.patient_model = patient_model

    def parameterize_psychsim_agents(self):
        super(Ijcai_4_3, self).parameterize_psychsim_agents()

        # default parameters for all agents
        for agent in self.agent_converter.all_psychsim_agents():
            agent.ps_agent.setHorizon(12)

    def add_reward_function(self):
    # set the reward function for manufacturers
        mn1 = self.agent_converter.get_psychsim_agent(self.mn1)
        mn1.tot_bklog_after_alloc_rwd_weight = 10.0
        mn1.inv_after_alloc_rwd_weight = 1.0
        mn1.allocate_rwd_weight = 5.0

        mn1.ps_agent.setReward(minimizeFeature(mn1.inv_after_allocation), mn1.inv_after_alloc_rwd_weight)
        mn1.ps_agent.setReward(
            minimizeFeature(mn1.total_backlog_after_allocation), mn1.tot_bklog_after_alloc_rwd_weight)
        for d in mn1.down_stream_agents:
            mn1.ps_agent.setReward(maximizeFeature(mn1.allocate_to[d]), mn1.allocate_rwd_weight)

        mn2 = self.agent_converter.get_psychsim_agent(self.mn2)
        mn2.tot_bklog_after_alloc_rwd_weight = 10.0
        mn2.inv_after_alloc_rwd_weight = 1.0
        mn2.allocate_rwd_weight = 5.0

        mn2.ps_agent.setReward(minimizeFeature(mn2.inv_after_allocation), mn2.inv_after_alloc_rwd_weight)
        mn2.ps_agent.setReward(
            minimizeFeature(mn2.total_backlog_after_allocation), mn2.tot_bklog_after_alloc_rwd_weight)
        for d in mn2.down_stream_agents:
            mn2.ps_agent.setReward(maximizeFeature(mn2.allocate_to[d]), mn2.allocate_rwd_weight)

        # set the reward function of the distributors
        ds1 = self.agent_converter.get_psychsim_agent(self.ds1)
        ds1.tot_bklog_after_alloc_rwd_weight = 10.0
        ds1.inv_after_alloc_rwd_weight = 1.0
        ds1.allocate_rwd_weight = 5.0
        ds1.ps_agent.setReward(minimizeFeature(ds1.inv_after_allocation), ds1.inv_after_alloc_rwd_weight)
        ds1.ps_agent.setReward(
            minimizeFeature(ds1.total_backlog_after_allocation), ds1.tot_bklog_after_alloc_rwd_weight)
        for d in ds1.down_stream_agents:
            ds1.ps_agent.setReward(maximizeFeature(ds1.allocate_to[d]), ds1.allocate_rwd_weight)

        # ds2
        ds2 = self.agent_converter.get_psychsim_agent(self.ds2)
        ds2.tot_bklog_after_alloc_rwd_weight = 10.0
        ds2.inv_after_alloc_rwd_weight = 1.0
        ds2.allocate_rwd_weight = 5.0
        ds2.ps_agent.setReward(minimizeFeature(ds2.inv_after_allocation), ds2.inv_after_alloc_rwd_weight)
        ds2.ps_agent.setReward(
            minimizeFeature(ds2.total_backlog_after_allocation), ds2.tot_bklog_after_alloc_rwd_weight)
        for d in ds2.down_stream_agents:
            ds2.ps_agent.setReward(maximizeFeature(ds2.allocate_to[d]), ds2.allocate_rwd_weight)

        # set reward for health-centers
        hc1 = self.agent_converter.get_psychsim_agent(self.hc1)
        hc1.inv_rwd_weight = 1.0
        hc1.lost_urgent_rwd_weight = 100.0
        hc1.lost_non_urgent_rwd_weight = 10.0

        hc1.ps_agent.setReward(minimizeFeature(hc1.inv_after_allocation), hc1.inv_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.lost_urgent), hc1.lost_urgent_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.backlog), hc1.lost_non_urgent_rwd_weight)

        hc2 = self.agent_converter.get_psychsim_agent(self.hc2)
        hc2.inv_rwd_weight = 1.0
        hc2.lost_urgent_rwd_weight = 100.0
        hc2.lost_non_urgent_rwd_weight = 10.0

        hc2.ps_agent.setReward(minimizeFeature(hc2.inv_after_allocation), hc2.inv_rwd_weight)
        hc2.ps_agent.setReward(minimizeFeature(hc2.lost_urgent), hc2.lost_urgent_rwd_weight)
        hc2.ps_agent.setReward(minimizeFeature(hc2.backlog), hc2.lost_non_urgent_rwd_weight)

    def add_health_centers_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim health centers.
        Default is allocate proportional, order up-to, order split by trust, update trust by history.
        """
        hc1 = self.simulation.health_centers[0]
        hc2 = self.simulation.health_centers[1]

        available_recipes_1 = {
            ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
            # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
            # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
            ORDER_AMOUNT: [HospitalUpToOrderAmountRecipe('1ord_up')],
            ORDERING_SPLIT: [HospitalOrderSplitByTrustRecipe('split_trust')],
            TRUST: [HospitalAsymmetricUpdateTrustRecipe('upd_trust_hist', 0.95, 0.5)]
        }

        # creates state-action dynamics
        agent = self.agent_converter.get_psychsim_agent(hc1)
        agent.define_state_dynamics(available_recipes_1)

        available_recipes_2 = {
            ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
            # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
            # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
            ORDER_AMOUNT: [HospitalUpToOrderAmountRecipe('1ord_up')],
            ORDERING_SPLIT: [HospitalOrderSplitEquallyRecipe('split_eq')],
            TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
        }

        # creates state-action dynamics
        agent = self.agent_converter.get_psychsim_agent(hc2)
        agent.define_state_dynamics(available_recipes_2)

    def add_distributors_recipes(self):

        # sets up ordering recipes
        allocate_proportional = DistributorAllocateProportionalRecipe('1alloc_prop')
        allocate_prefer1 = DistributorAllocateAgentPreferOneRecipe('alloc_prefer_one')
        allocate_prefer2 = DistributorAllocateAgentPreferTwoRecipe('alloc_prefer_two')

        allocate_recipes_planning = [allocate_proportional, allocate_prefer1, allocate_prefer2]
        allocate_recipes_no_planning = [allocate_proportional]

        # gets distributors with planning capacity
        planning_ds = []
        for idx in self.dist_lookahead_idxs:
            planning_ds.append(self.simulation.distributors[idx])

        # adds corresponding recipes
        for ds in self.simulation.distributors:

            allocation_recipes = allocate_recipes_no_planning
            if ds in planning_ds:
                allocation_recipes = allocate_recipes_planning

            available_recipes = {
                ALLOCATION: allocation_recipes,
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: [DistributorUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_eq')],
                TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)

    def parameterize_sim_agents(self):
        """
        sets up-to level for 2X2X2 complete network
        """
        # default parametrization
        super(Ijcai_4_3, self).parameterize_sim_agents()
        now = self.simulation.now
        for agent in self.simulation.agents:
            agent.up_to_level = agent.order_up_to_level_calculator.calculate(now)

