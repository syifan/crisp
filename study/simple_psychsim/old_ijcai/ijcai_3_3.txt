SCENARIO GOAL:
- Test if distributor who has the option to choose from {allocate proportionally and prefer HC1, prefer HC2},
  will it treat health-center1 which is bigger than health-center2 differently

THIS CODE
- distributor reward = Maximize 5*allocation - 10*backlog – inventory
- HC1 220, HC2 20


NETWORK STRUCTURE
- nodes:
    2 manufacturers (mn1 and mn2)
    2 distributors (ds1 and ds2)
    2 health centers (hc1 and hc2)
- network connections:
    (mn1, ds1)
    (mn2, ds2)
    (ds1, hc1)
    (ds1, hc2)
    (ds2, hc1)
    (ds2, hc2)

DEMAND PROFILE
- Health-center1 has 220 non-urgent patients (patient will be backlogged if not satisfied)
- Health-center2 has 20 non-urgent patients (patient will be backlogged if not satisfied)

ACTIONS
- Health Center [order up-to, split by trust, allocate proportional]
- Distributors [order up-to, split equally, (allocate proportional or prefer HC1 or prefer HC2)]
- Manufacturers [produce up-to, allocate proportional]


DISRUPTION
- mn1 has no disruption
- mn2 has disruption between 20 and 30, its capacity reduces to 85% of the original capacity

PERCEPTION
- all agents have perfect model of each other

PLANNING
- ds1, ds2, or both have forward planning / lookahead capacity
- planning horizon = 6 (12 including cleaning agent)