SCENARIO GOAL:
- Test if distributor who has the option to choose from {allocate proportionally and prefer HC1, prefer HC2},
  will it treat health-center1 which orders based on trust and health-center2 which orders equally to both of its upstream differently

THIS CODE
- distributor reward = Maximize allocation


NETWORK STRUCTURE
- nodes:
    2 manufacturers (mn1 and mn2)
    2 distributors (ds1 and ds2)
    2 health centers (hc1 and hc2)
- network connections:
    (mn1, ds1)
    (mn2, ds2)
    (ds1, hc1)
    (ds1, hc2)
    (ds2, hc1)
    (ds2, hc2)

DEMAND PROFILE
- both have 120 non-urgent patients (patient will be backlogged if not satisfied)

ACTIONS
- Health Center1 [order up-to, split by trust, allocate proportional]
- Health Center1 [order up-to, split equally, allocate proportional]
- Distributors [order up-to, split equally, (allocate proportional or prefer HC1 or prefer HC2)]
- Manufacturers [produce up-to, allocate proportional]


DISRUPTION
- mn1 has no disruption
- mn2 has disruption between 20 and 30, its capacity reduces to 85% of the original capacity

PERCEPTION
- all agents have perfect model of each other

PLANNING
- ds1, ds2, or both have forward planning / lookahead capacity
- planning horizon = 6 (12 including cleaning agent)