import os
import sys
import time
import datetime

sys.path.insert(0, '/home/doroudi.r/crisp')
# sys.path.insert(0, '/root/crisp')
from simulator.distruption import *
from simulator.psychsim_decision_maker import *
from simulator.simulation_runner import SimulationRunner
from multiprocessing import freeze_support

# import matplotlib

from study.simple_psychsim.ds_prfct_delta_both_alloc import *
from study.simple_psychsim.ds_prfct_assym_trust_alloc import *



# matplotlib.use('agg')
# import matplotlib.pyplot as plt
# import seaborn as sns

STABILIZED_PERIOD = 50  # defines period in which network values are stabilized

SNS_CONTEXT = 'paper'  # talk paper
SNS_STYLE = 'whitegrid'  # whitegrid darkgrid
PALETTE = 'colorblind'
IMG_FORMAT = '.png'  # .pdf .png
TITLE_SIZE = '12'
AXES_LABEL_SIZE = '10'
LEGEND_LABEL_SIZE = '10'
GRID_LINE_STYLE = '--'
GRID_COLOR = '.85'
DISRUPT_ALPHA = 0.3
DISRUPT_COLOR = 'skyblue'

ALLOCATION_RECIPE = 'Allocation recipe'
ORDER_SPLIT_RECIPE = 'Order split recipe'
ORDER_AMT_RECIPE = 'Order amount recipe'
PROD_AMT_RECIPE = 'Production amount recipe'

IN_PRODUCTION = 'In production'
BACKLOG = 'Backlog'
UP_TO_LEVEL = 'Up-to-level'
ORDER = 'order'
ORDER_TITLE = 'Order'
LOSS = 'Loss'
SAT_URGENT = 'Satisfied Urgent'
SAT_NON_URGENT = 'Satisfied Non-Urgent'
DEMAND = 'Demand'
INVENTORY = 'Inventory'
INCOMING_ORDER = 'incoming_order'
TRUST = 'Trust'
DELIVERY_RATE = 'Deliv. rate'
REWARD = 'Reward'
ALLOCATE = 'allocate'
ALLOCATE_TITLE = 'Allocation'

ACTION_VALUES_POSTFIX = ' action values'
SELECTED_RECIPE_POSTFIX = ' selected recipes'
STATE_INFO_POSTFIX = ' state'
SHIPPING_INFO_POSTFIX = ' shipments'
TRUST_INFO_POSTFIX = ' trust'

UNIT = 'Unit'
AGENT = 'Agent'
VALUE = 'Value'
ITEM = 'item'
TIME = 'Time'


# def set_sns_styles():
#     sns.set_context(SNS_CONTEXT)
#     sns.set_style(SNS_STYLE, {
#         # 'axes.edgecolor': '.2',
#         'grid.color': GRID_COLOR,
#         'grid.linestyle': GRID_LINE_STYLE,
#         # 'axes.grid': True
#     })
#     sns.color_palette(PALETTE)

# HERE
# def set_plot_style(simulation, data, ax, title, auto_min=False):
#     sns.despine(left=True, bottom=True)
#     plt.title(title, weight='bold').set_fontsize(TITLE_SIZE)
#     plt.xlabel(TIME, size=AXES_LABEL_SIZE, fontweight='bold')
#     plt.ylabel(VALUE, size=AXES_LABEL_SIZE, fontweight='bold')
#     plt.legend(title='', fontsize=LEGEND_LABEL_SIZE, frameon=True, facecolor='white')
#     ax.grid(axis='x')
#
#     # adds disruption shaded areas
#     for disruption in simulation.disruptions:
#         if isinstance(disruption, LineShutDownDisruption):
#             plt.axvspan(disruption.happen_day_1, disruption.end_day_1, facecolor=DISRUPT_COLOR, alpha=DISRUPT_ALPHA)
#             plt.axvspan(disruption.happen_day_2, disruption.end_day_2, facecolor=DISRUPT_COLOR, alpha=DISRUPT_ALPHA)
#         elif isinstance(disruption, RecallDisruption):
#             plt.axvspan(disruption.happen_day, disruption.happen_day + 1, facecolor=DISRUPT_COLOR, alpha=DISRUPT_ALPHA)
#         elif isinstance(disruption, DemandChangeDisruption):
#             plt.axvspan(disruption.start_time, disruption.end_time + 1, facecolor=DISRUPT_COLOR, alpha=DISRUPT_ALPHA)
#
#     # determines min and max y after stabilization
#     if not auto_min: return
#     min_y = min(data[data['Time'] > STABILIZED_PERIOD]['Value'])
#     max_y = max(data[data['Time'] > STABILIZED_PERIOD]['Value'])
#     diff = (max_y - min_y) * 0.1
#     if diff == 0: return
#     min_y -= diff
#     max_y += diff
#     plt.ylim(ymin=min_y, ymax=max_y)
# #
# #
# def print_agent_data(simulation, agent_data, agent_id, results_path, postfix, auto_min=False):
#     plt.figure()
#     ax = sns.tsplot(agent_data, time=TIME, condition=ITEM, value=VALUE, unit=UNIT)
#     set_plot_style(simulation, agent_data, ax, agent_id + postfix, auto_min)
#     plt.savefig(os.path.join(results_path, agent_id + postfix + IMG_FORMAT), dpi=600)
#     plt.close()
# #
# #
# def print_item_data(simulation, item_data, item_id, results_path, auto_min=False):
#     plt.figure()
#     ax = sns.tsplot(item_data, time=TIME, condition=AGENT, value=VALUE, unit=UNIT)
#     set_plot_style(simulation, item_data, ax, item_id, auto_min)
#     plt.savefig(os.path.join(results_path, item_id + IMG_FORMAT), dpi=600)
#     plt.close()


def get_agent_data(data, agent_id):
    return data[data[AGENT] == agent_id]


def calculate_aggregate_data(aggregated_data_file, shipping_data, state_data):
    with open(aggregated_data_file, "w") as text_file:
        total_inv_hc1 = state_data[(state_data['Agent'] == 'HC 1') & (state_data['item'] == 'Inventory')]['Value'][
                        STABILIZED_PERIOD:].sum()
        text_file.write("HC1 total inventory: %s\n" % total_inv_hc1)
        total_bl_hc1 = state_data[(state_data['Agent'] == 'HC 1') & (state_data['item'] == 'Backlog')]['Value'][
                       STABILIZED_PERIOD:].sum()
        text_file.write("HC1 total backlog: %s\n" % total_bl_hc1)
        total_loss_hc1 = state_data[(state_data['Agent'] == 'HC 1') & (state_data['item'] == 'Loss')]['Value'][
                         STABILIZED_PERIOD:].sum()
        text_file.write("HC1 total loss: %s\n" % total_loss_hc1)
        hc1_total_cost = 100 * total_loss_hc1 + 100 * total_bl_hc1 + total_inv_hc1
        text_file.write("HC1 total cost: %s\n" % hc1_total_cost)
        hc1_order_ds1 = shipping_data[(shipping_data['Agent'] == 'HC 1') & (shipping_data['item'] == 'Order DS 1')][
                            'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("HC1 order DS1: %s\n" % hc1_order_ds1)
        hc1_order_ds2 = shipping_data[(shipping_data['Agent'] == 'HC 1') & (shipping_data['item'] == 'Order DS 2')][
                            'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("HC1 order DS2: %s\n" % hc1_order_ds2)
        hc1_sat_urgent = \
            shipping_data[(shipping_data['Agent'] == 'HC 1') & (shipping_data['item'] == 'Satisfied Urgent')][
                'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("HC1 Urgent On-Time Satisfied: %s\n" % hc1_sat_urgent)
        hc1_sat_non_urgent = \
            shipping_data[(shipping_data['Agent'] == 'HC 1') & (shipping_data['item'] == 'Satisfied Non-Urgent')][
                'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("HC1 Non-Urgent On-Time Satisfied: %s\n" % hc1_sat_non_urgent)
        ### ---
        total_inv_hc2 = state_data[(state_data['Agent'] == 'HC 2') & (state_data['item'] == 'Inventory')]['Value'][
                        STABILIZED_PERIOD:].sum()
        text_file.write("HC2 total inventory: %s\n" % total_inv_hc2)
        total_bl_hc2 = state_data[(state_data['Agent'] == 'HC 2') & (state_data['item'] == 'Backlog')]['Value'][
                       STABILIZED_PERIOD:].sum()
        text_file.write("HC2 total backlog: %s\n" % total_bl_hc2)
        total_loss_hc2 = state_data[(state_data['Agent'] == 'HC 2') & (state_data['item'] == 'Loss')]['Value'][
                         STABILIZED_PERIOD:].sum()
        text_file.write("HC2 total Loss: %s\n" % total_loss_hc2)
        hc2_total_cost = 100 * total_loss_hc2 + 100 * total_bl_hc2 + total_inv_hc2
        text_file.write("HC2 total cost: %s\n" % hc2_total_cost)
        hc2_order_ds1 = shipping_data[(shipping_data['Agent'] == 'HC 2') & (shipping_data['item'] == 'Order DS 1')][
                            'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("HC2 order DS1: %s\n" % hc2_order_ds1)
        hc2_order_ds2 = shipping_data[(shipping_data['Agent'] == 'HC 2') & (shipping_data['item'] == 'Order DS 2')][
                            'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("HC2 order DS2: %s\n" % hc2_order_ds2)
        hc2_sat_urgent = \
            shipping_data[(shipping_data['Agent'] == 'HC 2') & (shipping_data['item'] == 'Satisfied Urgent')][
                'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("HC2 Urgent On-Time Satisfied: %s\n" % hc2_sat_urgent)

        hc2_sat_non_urgent = \
            shipping_data[(shipping_data['Agent'] == 'HC 2') & (shipping_data['item'] == 'Satisfied Non-Urgent')][
                'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("HC2 Non-Urgent On-Time Satisfied: %s\n" % hc2_sat_non_urgent)
        ### ----
        total_inv_ds1 = state_data[(state_data['Agent'] == 'DS 1') & (state_data['item'] == 'Inventory')]['Value'][
                        STABILIZED_PERIOD:].sum()
        text_file.write("DS1 total inventory: %s\n" % total_inv_ds1)
        total_bl_ds1 = state_data[(state_data['Agent'] == 'DS 1') & (state_data['item'] == 'Backlog')]['Value'][
                       STABILIZED_PERIOD:].sum()
        text_file.write("DS1 total backlog: %s\n" % total_bl_ds1)
        ds1_ship_hc1 = shipping_data[(shipping_data['Agent'] == 'DS 1') & (shipping_data['item'] == 'Allocation HC 1')][
                           'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("DS1 shipment HC1: %s\n" % ds1_ship_hc1)
        ds1_ship_hc2 = shipping_data[(shipping_data['Agent'] == 'DS 1') & (shipping_data['item'] == 'Allocation HC 2')][
                           'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("DS1 shipment HC2: %s\n" % ds1_ship_hc2)
        ds1_total_cost = 10 * total_bl_ds1 + total_inv_ds1 - 5 * (ds1_ship_hc1 + ds1_ship_hc2)
        text_file.write("DS1 total cost: %s\n" % ds1_total_cost)
        ds1_order_mn1 = shipping_data[(shipping_data['Agent'] == 'DS 1') & (shipping_data['item'] == 'Order MN 1')][
                            'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("DS1 order MN1: %s\n" % ds1_order_mn1)
        ds1_order_mn2 = shipping_data[(shipping_data['Agent'] == 'DS 1') & (shipping_data['item'] == 'Order MN 2')][
                            'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("DS1 order MN2: %s\n" % ds1_order_mn2)
        ### ----
        total_inv_ds2 = state_data[(state_data['Agent'] == 'DS 2') & (state_data['item'] == 'Inventory')]['Value'][
                        STABILIZED_PERIOD:].sum()
        text_file.write("DS2 total inventory: %s\n" % total_inv_ds2)
        total_bl_ds2 = state_data[(state_data['Agent'] == 'DS 2') & (state_data['item'] == 'Backlog')]['Value'][
                       STABILIZED_PERIOD:].sum()
        text_file.write("DS2 total backlog: %s\n" % total_bl_ds2)
        ds2_ship_hc1 = shipping_data[(shipping_data['Agent'] == 'DS 2') & (shipping_data['item'] == 'Allocation HC 1')][
                           'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("DS2 shipment HC1: %s\n" % ds2_ship_hc1)
        ds2_ship_hc2 = shipping_data[(shipping_data['Agent'] == 'DS 2') & (shipping_data['item'] == 'Allocation HC 2')][
                           'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("DS2 shipment HC2: %s\n" % ds2_ship_hc2)
        ds2_total_cost = 10 * total_bl_ds2 + total_inv_ds2 - 5 * (ds2_ship_hc1 + ds2_ship_hc2)
        text_file.write("DS2 total cost: %s\n" % ds2_total_cost)
        ds2_order_mn1 = shipping_data[(shipping_data['Agent'] == 'DS 2') & (shipping_data['item'] == 'Order MN 1')][
                            'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("DS2 order MN1: %s\n" % ds2_order_mn1)
        ds2_order_mn2 = shipping_data[(shipping_data['Agent'] == 'DS 2') & (shipping_data['item'] == 'Order MN 2')][
                            'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("DS2 order MN2: %s\n" % ds2_order_mn2)
        ### ----
        total_inv_mn1 = state_data[(state_data['Agent'] == 'MN 1') & (state_data['item'] == 'Inventory')]['Value'][
                        STABILIZED_PERIOD:].sum()
        text_file.write("MN1 total inventory: %s\n" % total_inv_mn1)
        total_bl_mn1 = state_data[(state_data['Agent'] == 'MN 1') & (state_data['item'] == 'Backlog')]['Value'][
                       STABILIZED_PERIOD:].sum()
        text_file.write("MN1 total backlog: %s\n" % total_bl_mn1)
        mn1_ship_ds1 = shipping_data[(shipping_data['Agent'] == 'MN 1') & (shipping_data['item'] == 'Allocation DS 1')][
                           'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("MN1 shipment DS1: %s\n" % mn1_ship_ds1)
        mn1_ship_ds2 = shipping_data[(shipping_data['Agent'] == 'MN 1') & (shipping_data['item'] == 'Allocation DS 2')][
                           'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("MN1 shipment DS2: %s\n" % mn1_ship_ds2)
        mn1_total_cost = 10 * total_bl_mn1 + total_inv_mn1 - 5 * (mn1_ship_ds1 + mn1_ship_ds2)
        text_file.write("MN1 total cost: %s\n" % mn1_total_cost)
        ### ----
        total_inv_mn2 = state_data[(state_data['Agent'] == 'MN 2') & (state_data['item'] == 'Inventory')]['Value'][
                        STABILIZED_PERIOD:].sum()
        text_file.write("MN2 total inventory: %s\n" % total_inv_mn2)
        total_bl_mn2 = state_data[(state_data['Agent'] == 'MN 2') & (state_data['item'] == 'Backlog')]['Value'][
                       STABILIZED_PERIOD:].sum()
        text_file.write("MN2 total backlog: %s\n" % total_bl_mn2)
        mn2_ship_ds1 = shipping_data[(shipping_data['Agent'] == 'MN 2') & (shipping_data['item'] == 'Allocation DS 1')][
                           'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("MN2 shipment DS1: %s\n" % mn2_ship_ds1)
        mn2_ship_ds2 = shipping_data[(shipping_data['Agent'] == 'MN 2') & (shipping_data['item'] == 'Allocation DS 2')][
                           'Value'][STABILIZED_PERIOD:].sum()
        text_file.write("MN2 shipment DS2: %s\n" % mn2_ship_ds2)
        mn2_total_cost = 10 * total_bl_mn2 + total_inv_mn2 - 5 * (mn2_ship_ds1 + mn2_ship_ds2)
        text_file.write("MN2 total cost: %s\n" % mn2_total_cost)


def run_profile(profile):
    print 'Running profile ' + profile.name + '...'

    # creates decision maker
    decision_maker = PsychSimDecisionMaker(profile.world, profile.get_all_psychsim_agents())

    # creates the simulation runner
    runner = SimulationRunner(profile.simulation, decision_maker)

    # creates output directory
    results_path = os.path.join('results', profile.name)
    if not os.path.exists(results_path):
        os.makedirs(results_path)

    # set_sns_styles()

    total_time = time.time()

    distributors = [profile.ds1, profile.ds2]
    # distributors = [profile.ds1]
    # health_centers = [profile.hc1]
    health_centers = [profile.hc1, profile.hc2]
    # manufacturers = [profile.mn1]
    manufacturers = [profile.mn1, profile.mn2]
    all_agents = []
    all_agents.extend(distributors)
    all_agents.extend(health_centers)
    all_agents.extend(manufacturers)
    agent_ids = {ag: profile.agent_converter.get_psychsim_agent(ag).name for ag in all_agents}

    # sets up data collection
    # log_file = open('log_file.json','w')
    data_columns = [TIME, AGENT, ITEM, VALUE, UNIT]
    state_data = pd.DataFrame(columns=data_columns)
    shipping_data = pd.DataFrame(columns=data_columns)
    trust_data = pd.DataFrame(columns=data_columns)
    sel_recipes_data = pd.DataFrame(columns=data_columns)
    act_values_data = pd.DataFrame(columns=data_columns)
    rewards_data = pd.DataFrame(columns=data_columns)

    # runs the simulation, one cycle at the time
    for i in range(profile.time_periods):

        start = time.clock()
        print 'Cycle ' + str(i) + '...'
        # runs one simulation and decision-making cycle
        runner.next_cycle()

        state_data_matrix = []
        shipping_data_matrix = []
        trust_data_matrix = []
        sel_recipes_data_matrix = []
        act_values_data_matrix = []
        rewards_data_matrix = []

        # collects health centers' data
        for hc in health_centers:

            hc_name = agent_ids[hc]
            hc_chosen_recipe = decision_maker.recipes_chosen[hc_name]
            hc_order_amt_recipe = -1
            if 'ord_up' in hc_chosen_recipe:
                hc_order_amt_recipe = 1
            elif 'ord+' in hc_chosen_recipe:
                hc_order_amt_recipe = 2
            elif 'ord-' in hc_chosen_recipe:
                hc_order_amt_recipe = 0
            else:
                print 'error hc order amnt'

            hc_order_split_recipe = -1
            if 'split_trust' in hc_chosen_recipe:
                hc_order_split_recipe = 1
            elif 'split_eq' in hc_chosen_recipe:
                hc_order_split_recipe = 0
            else:
                print 'error hc order split'

            state_data_matrix.append([i, hc_name, INVENTORY, hc.inventory_level(), ''])
            state_data_matrix.append(
                [i, hc_name, DEMAND, hc.urgent + hc.satisfied_urgent + hc.non_urgent, ''])
            state_data_matrix.append([i, hc_name, LOSS, hc.urgent, ''])
            state_data_matrix.append([i, hc_name, BACKLOG, hc.backlog_non_urgent, ''])
            state_data_matrix.append(
                [i, hc_name, ORDER_TITLE, sum(order.amount for order in hc.get_history_item(i + 1)[ORDER]), ''])
            state_data_matrix.append([i, hc_name, UP_TO_LEVEL, hc.up_to_level, ''])

            for order in hc.get_history_item(i + 1)[ORDER]:
                shipping_data_matrix.append([i, hc_name, ORDER_TITLE + ' ' + agent_ids[order.dst], order.amount, ''])
            shipping_data_matrix.append([i, hc_name, SAT_URGENT, hc.satisfied_urgent, ''])
            shipping_data_matrix.append([i, hc_name, SAT_NON_URGENT, hc.satisfied_non_urgent, ''])

            for sim_upst in decision_maker.trust[hc_name].keys():
                trust_data_matrix.append(
                    [i, hc_name, TRUST + ' ' + sim_upst, decision_maker.trust[hc_name][sim_upst], ''])
                u = profile.agent_converter.get_sim_agent_from_psychsim_id(sim_upst)
                trust_data_matrix.append([i, hc_name, DELIVERY_RATE + ' ' + sim_upst, hc.ontime_deliv_rate[u], ''])

            sel_recipes_data_matrix.append([i, hc_name, ORDER_AMT_RECIPE, hc_order_amt_recipe, ''])
            sel_recipes_data_matrix.append([i, hc_name, ORDER_SPLIT_RECIPE, hc_order_split_recipe, ''])
            for action, value in decision_maker.action_values[hc_name].items():
                act_values_data_matrix.append([i, hc_name, action, value, ''])
            rewards_data_matrix.append([i, hc_name, REWARD, decision_maker.rewards[hc_name], ''])

        # collects distributors' data
        for ds in distributors:

            ds_name = agent_ids[ds]
            ds_chosen_recipe = decision_maker.recipes_chosen[ds_name]
            ds_order_amt_recipe = -1
            if 'ord_up' in ds_chosen_recipe:
                ds_order_amt_recipe = 1
            elif 'ord+' in ds_chosen_recipe:
                ds_order_amt_recipe = 2
            elif 'ord-' in ds_chosen_recipe:
                ds_order_amt_recipe = 0
            else:
                print 'error ds order amnt'

            ds_order_split_recipe = -1
            if 'split_trust' in ds_chosen_recipe:
                ds_order_split_recipe = 1
            elif 'split_eq' in ds_chosen_recipe:
                ds_order_split_recipe = 0
            else:
                print 'error ds order split'

            ds_alloc_recipe = -1
            if 'alloc_prop' in ds_chosen_recipe:
                ds_alloc_recipe = 0

            elif 'alloc_prefer_two' in ds_chosen_recipe:
                ds_alloc_recipe = 2

            elif 'alloc_prefer_one' in ds_chosen_recipe:
                ds_alloc_recipe = 1

            elif 'alloc_eq' in ds_chosen_recipe:
                ds_alloc_recipe = 0
            else:
                print 'error ds allocation'

            state_data_matrix.append([i, ds_name, INVENTORY, ds.inventory_level(), ''])
            state_data_matrix.append([i, ds_name, BACKLOG, sum(bl.amount for bl in ds.backlog), ''])
            state_data_matrix.append(
                [i, ds_name, ORDER_TITLE, sum(order.amount for order in ds.get_history_item(i + 1)[ORDER]), ''])
            state_data_matrix.append(
                [i, ds_name, DEMAND, sum(order.amount for order in ds.get_history_item(i + 1)[INCOMING_ORDER]), ''])
            state_data_matrix.append([i, ds_name, UP_TO_LEVEL, ds.up_to_level, ''])

            orders = ds.get_history_item(i + 1)[ORDER]
            if len(orders) == 0:
                for mn in ds.upstream_nodes:
                    shipping_data_matrix.append([i, ds_name, ORDER_TITLE + ' ' + agent_ids[mn], 0, ''])
            else:
                for order in orders:
                    shipping_data_matrix.append(
                        [i, ds_name, ORDER_TITLE + ' ' + agent_ids[order.dst], order.amount, ''])
            orders = ds.get_history_item(i + 1)[ALLOCATE]
            if len(orders) == 0:
                for hc in ds.downstream_nodes:
                    shipping_data_matrix.append([i, ds_name, ALLOCATE_TITLE + ' ' + agent_ids[hc], 0, ''])
            else:
                hc_orders = {}
                for order in orders:
                    hc = agent_ids[order[ORDER].src]
                    if hc in hc_orders:
                        hc_orders[hc] += order[ITEM].amount
                    else:
                        hc_orders[hc] = order[ITEM].amount
                for hc, amount in hc_orders.iteritems():
                    shipping_data_matrix.append([i, ds_name, ALLOCATE_TITLE + ' ' + hc, amount, ''])

            for sim_upst in decision_maker.trust[ds_name].keys():
                trust_data_matrix.append(
                    [i, ds_name, TRUST + ' ' + sim_upst, decision_maker.trust[ds_name][sim_upst], ''])
                u = profile.agent_converter.get_sim_agent_from_psychsim_id(sim_upst)
                trust_data_matrix.append([i, ds_name, DELIVERY_RATE + ' ' + sim_upst, ds.ontime_deliv_rate[u], ''])

            sel_recipes_data_matrix.append([i, ds_name, ORDER_AMT_RECIPE, ds_order_amt_recipe, ''])
            sel_recipes_data_matrix.append([i, ds_name, ORDER_SPLIT_RECIPE, ds_order_split_recipe, ''])
            sel_recipes_data_matrix.append([i, ds_name, ALLOCATION_RECIPE, ds_alloc_recipe, ''])
            for action, value in decision_maker.action_values[ds_name].items():
                act_values_data_matrix.append([i, ds_name, action, value, ''])
            rewards_data_matrix.append([i, ds_name, REWARD, decision_maker.rewards[ds_name], ''])

        # collects manufacturers' data
        for mn in manufacturers:

            mn_name = agent_ids[mn]
            mn_chosen_recipe = decision_maker.recipes_chosen[mn_name]
            mn_prod_amt_recipe = -1
            if 'prod_pro' in mn_chosen_recipe:
                mn_prod_amt_recipe = 1
            elif 'prod+' in mn_chosen_recipe:
                mn_prod_amt_recipe = 2
            elif 'prod-' in mn_chosen_recipe:
                mn_prod_amt_recipe = 0
            else:
                print 'error prod amount'

            mn_alloc_recipe = -1
            if 'alloc_prop' in mn_chosen_recipe:
                mn_alloc_recipe = 1
            elif 'alloc_eq' in mn_chosen_recipe:
                mn_alloc_recipe = 0
            else:
                print 'error mn allocation'

            state_data_matrix.append([i, mn_name, INVENTORY, mn.inventory_level(), ''])
            state_data_matrix.append([i, mn_name, BACKLOG, sum(bl.amount for bl in mn.backlog), ''])
            state_data_matrix.append(
                [i, mn_name, IN_PRODUCTION, sum(in_prod.amount for in_prod in mn.in_production), ''])
            state_data_matrix.append([i, mn_name, UP_TO_LEVEL, mn.up_to_level, ''])
            state_data_matrix.append(
                [i, mn_name, DEMAND, sum(order.amount for order in mn.get_history_item(i + 1)[INCOMING_ORDER]), ''])

            orders = mn.get_history_item(i + 1)[ALLOCATE]
            if len(orders) == 0:
                for ds in mn.downstream_nodes:
                    shipping_data_matrix.append([i, mn_name, ALLOCATE_TITLE + ' ' + agent_ids[ds], 0, ''])
            else:
                dist_orders = {}
                for order in orders:
                    dist = agent_ids[order[ORDER].src]
                    if dist in dist_orders:
                        dist_orders[dist] += order[ITEM].amount
                    else:
                        dist_orders[dist] = order[ITEM].amount
                for dist, amount in dist_orders.iteritems():
                    shipping_data_matrix.append([i, mn_name, ALLOCATE_TITLE + ' ' + dist, amount, ''])

            sel_recipes_data_matrix.append([i, mn_name, PROD_AMT_RECIPE, mn_prod_amt_recipe, ''])
            sel_recipes_data_matrix.append([i, mn_name, ALLOCATION_RECIPE, mn_alloc_recipe, ''])
            for action, value in decision_maker.action_values[mn_name].items():
                act_values_data_matrix.append([i, mn_name, action, value, ''])
            rewards_data_matrix.append([i, mn_name, REWARD, decision_maker.rewards[mn_name], ''])

        # appends data
        state_data = state_data.append(pd.DataFrame(state_data_matrix, columns=data_columns))
        shipping_data = shipping_data.append(pd.DataFrame(shipping_data_matrix, columns=data_columns))
        trust_data = trust_data.append(pd.DataFrame(trust_data_matrix, columns=data_columns))
        sel_recipes_data = sel_recipes_data.append(pd.DataFrame(sel_recipes_data_matrix, columns=data_columns))
        act_values_data = act_values_data.append(pd.DataFrame(act_values_data_matrix, columns=data_columns))
        rewards_data = rewards_data.append(pd.DataFrame(rewards_data_matrix, columns=data_columns))

        # log_file.write(simulation.to_json())
        print str(time.clock() - start) + 's'

    total_time = time.time() - total_time
    print 'Total time: ' + str(total_time) + 's'

    # log_file.close()
    state_data.reset_index()
    shipping_data.reset_index()
    aggregated_data_file = os.path.join(results_path, profile.name + '.txt')
    calculate_aggregate_data(aggregated_data_file, shipping_data, state_data)
    trust_data.reset_index()
    sel_recipes_data.reset_index()
    act_values_data.reset_index()
    rewards_data.reset_index()

    # creates excel and writes all agents data while printing charts (HERE)
    state_writer = pd.ExcelWriter(os.path.join(results_path, 'state-data.xlsx'))
    action_writer = pd.ExcelWriter(os.path.join(results_path, 'action-data.xlsx'))

    for agent_id in agent_ids.values():
        agent_data = get_agent_data(state_data, agent_id)
        # print_agent_data(profile.simulation, agent_data, agent_id, results_path, STATE_INFO_POSTFIX)
        agent_data.to_excel(state_writer, agent_id + STATE_INFO_POSTFIX)

        agent_data = get_agent_data(shipping_data, agent_id)
        # print_agent_data(profile.simulation, agent_data, agent_id, results_path, SHIPPING_INFO_POSTFIX, True)
        agent_data.to_excel(state_writer, agent_id + SHIPPING_INFO_POSTFIX)

        agent_data = get_agent_data(trust_data, agent_id)
        if len(agent_data) > 0:
            # print_agent_data(profile.simulation, agent_data, agent_id, results_path, TRUST_INFO_POSTFIX)
            agent_data.to_excel(state_writer, agent_id + TRUST_INFO_POSTFIX)

        agent_data = get_agent_data(sel_recipes_data, agent_id)
        # print_agent_data(profile.simulation, agent_data, agent_id, results_path, SELECTED_RECIPE_POSTFIX)
        agent_data.to_excel(action_writer, agent_id + SELECTED_RECIPE_POSTFIX)

        agent_data = get_agent_data(act_values_data, agent_id)
        # print_agent_data(profile.simulation, agent_data, agent_id, results_path, ACTION_VALUES_POSTFIX, True)
        agent_data.to_excel(action_writer, agent_id + ACTION_VALUES_POSTFIX)

    inv_data = state_data[state_data[ITEM] == INVENTORY]
    # print_item_data(profile.simulation, inv_data, INVENTORY, results_path, True)

    # print_item_data(profile.simulation, rewards_data, REWARD, results_path, True)
    rewards_data.to_excel(state_writer, REWARD)

    state_writer.save()
    action_writer.save()


if __name__ == '__main__':
    freeze_support()
    # creates the simulation profiles
    # for i in range(0, 50):
    print 'start time is'
    print datetime.datetime.today()

    # profiles = []
    # delta1 = 0.5
    # delta2 = 0.5
    # horizon = 12
    # for i in range(0, 4):
    #     profile_name = "DS2_True_HC1_%0.3f_HC2_%0.3f_horizon%d_DS2Plan_initial_diff_900_lng%d" % (
    #     delta1, delta2, horizon,
    #     i)
    #     profile = DsPerfectDeltaBothHc(profile_name, delta1, delta2, horizon, [1])
    #     profiles.append(profile)

    alpha = 0.4
    beta = 0.5
    horizon = 12
    profiles = []
    for i in range(0, 4):
        profile_name = "DS2_True_BothDelta_Recov_%0.3f_Lose_%0.3f_horizon%d_DS2Plan_initial_pri_%d" % (alpha, beta,
                                                                                                          horizon, i)
        profile = DsPerfectAssymBothHc(profile_name, alpha, beta, horizon, [1])
        profiles.append(profile)


    print 'start time is'
    print datetime.datetime.today()
    for profile in profiles:
        # orig_stdout = sys.stdout
        # results_path = os.path.join('results', profile.name)
        # if not os.path.exists(results_path):
        #     os.makedirs(results_path)
        # f = open(os.path.join(results_path, 'explain.txt'), 'w')
        # sys.stdout = f
        run_profile(profile)
        # sys.stdout = orig_stdout
        # f.close()
    print 'end time is'
    print datetime.datetime.today()