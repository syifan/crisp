from simulator.distruption import LineShutDownDisruption
from study.simple_psychsim import *

class PerfectDistLookaheadNet12DiffHcOrd(SimulationProfileBase222):
    """
    A simulation profile for testing having two health-centers with one ordering based on trust and
     one ordering equally, testing if distributors will treat them differently.
    """

    def __init__(self, name, dist_lookahead_idxs):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list dist_lookahead_idxs: a list with the indexes of the distributors with planning capacity.
        """
        self.dist_lookahead_idxs = dist_lookahead_idxs
        super(PerfectDistLookaheadNet12DiffHcOrd, self).__init__(name)
        self.time_periods = 100

    def parameterize_agent_builder(self):
        """
        Sets the default parameters for the agent builder.
        """
        super(PerfectDistLookaheadNet12DiffHcOrd, self).parameterize_agent_builder()
        self.agent_builder.history_preserve_time = 10


    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 20
        manufacturer_shutdown.end_day_1 = 30
        manufacturer_shutdown.decrease_factor_1 = 0.85
        self.simulation.disruptions.append(manufacturer_shutdown)

    def define_agent_connections(self):
        """
        Collects the references to the agents in the network. Does not change connectivity.
        """
        self.hc1 = self.simulation.health_centers[0]
        self.hc2 = self.simulation.health_centers[1]
        self.ds1 = self.simulation.distributors[0]
        self.ds2 = self.simulation.distributors[1]
        self.mn1 = self.simulation.manufacturers[0]
        self.mn2 = self.simulation.manufacturers[1]

        for ds in self.simulation.distributors:

            # ds <-> hc
            for hc in self.simulation.health_centers:
                hc.upstream_nodes.append(ds)
                ds.downstream_nodes.append(hc)
        self.simulation.distributors[0].upstream_nodes.append(self.simulation.manufacturers[0])
        self.simulation.distributors[1].upstream_nodes.append(self.simulation.manufacturers[1])
        self.simulation.manufacturers[0].downstream_nodes.append(self.simulation.distributors[0])
        self.simulation.manufacturers[1].downstream_nodes.append(self.simulation.distributors[1])

    def add_patient_model(self):
        """
        Adds one patient model to generate demand at the health centers.
        """
        patient_model = ConstantPatientModel(self.simulation.health_centers)
        self.simulation.patient_model = patient_model
        patient_model.urgent = 120
        patient_model.non_urgent = 0

    def parameterize_psychsim_agents(self):
        super(PerfectDistLookaheadNet12DiffHcOrd, self).parameterize_psychsim_agents()

        # default parameters for all agents
        for agent in self.agent_converter.all_psychsim_agents():
            agent.ps_agent.setHorizon(12)

    def add_health_centers_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim health centers.
        Default is allocate proportional, order up-to, order split by trust, update trust by history.
        """
        hc1 = self.simulation.health_centers[0]
        hc2 = self.simulation.health_centers[1]

        available_recipes_1 = {
            ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
            # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
            # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
            ORDER_AMOUNT: [HospitalUpToOrderAmountRecipe('1ord_up')],
            ORDERING_SPLIT: [HospitalOrderSplitByTrustRecipe('split_trust')],
            # ORDERING_SPLIT: [HospitalOrderSplitEquallyRecipe('split_eq')],
            TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
        }

        # creates state-action dynamics
        agent = self.agent_converter.get_psychsim_agent(hc1)
        agent.define_state_dynamics(available_recipes_1)

        available_recipes_2 = {
            ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
            # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
            # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
            ORDER_AMOUNT: [HospitalUpToOrderAmountRecipe('1ord_up')],
            ORDERING_SPLIT: [HospitalOrderSplitByTrustRecipe('split_trust')],
            # ORDERING_SPLIT: [HospitalOrderSplitEquallyRecipe('split_eq')],
            TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
        }

        # creates state-action dynamics
        agent = self.agent_converter.get_psychsim_agent(hc2)
        agent.define_state_dynamics(available_recipes_2)

    def add_distributors_recipes(self):
        # sets up ordering recipes
        order_offset = 0.2
        order_up_to = DistributorUpToOrderAmountRecipe('1ord_up')
        order_more = DistributorUpToOrderAmountRecipe('ord+', order_offset)
        order_less = DistributorUpToOrderAmountRecipe('ord-', -order_offset)
        order_amt_recipes_planning = [order_up_to, order_less, order_more]
        order_amt_recipes_no_planning = [order_up_to]

        # gets distributors with planning capacity
        planning_ds = []
        for idx in self.dist_lookahead_idxs:
            planning_ds.append(self.simulation.distributors[idx])

        # adds corresponding recipes
        for ds in self.simulation.distributors:

            order_amt_recipes = order_amt_recipes_no_planning
            if ds in planning_ds:
                order_amt_recipes = order_amt_recipes_planning

            available_recipes = {
                ALLOCATION: [DistributorAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: order_amt_recipes,
                ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_eq')],
                TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)