from simulator.distruption import LineShutDownDisruption
from study.simple_psychsim import *


class PerfectMnLookaheadNet2DiffDist(SimulationProfileBase222):
    """
    A simulation profile for testing having two health-centers with one ordering based on trust and
     one ordering equally, testing if distributors will treat them differently.
    """

    def __init__(self, name, mn_lookahead_idxs):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list dist_lookahead_idxs: a list with the indexes of the distributors with planning capacity.
        """
        self.mn_lookahead_idxs = mn_lookahead_idxs
        super(PerfectMnLookaheadNet2DiffDist, self).__init__(name)
        self.time_periods = 60

    def parameterize_agent_builder(self):
        """
        Sets the default parameters for the agent builder.
        """
        super(PerfectMnLookaheadNet2DiffDist, self).parameterize_agent_builder()
        self.agent_builder.history_preserve_time = 10

    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 20
        manufacturer_shutdown.end_day_1 = 30
        manufacturer_shutdown.decrease_factor_1 = 0.85
        self.simulation.disruptions.append(manufacturer_shutdown)

    def add_patient_model(self):
        """
        Adds one patient model to generate demand at the health centers.
        """
        patient_model = ConstantPatientModel(self.simulation.health_centers)
        self.simulation.patient_model = patient_model
        patient_model.urgent = 120
        patient_model.non_urgent = 0

    def parameterize_psychsim_agents(self):
        super(PerfectMnLookaheadNet2DiffDist, self).parameterize_psychsim_agents()

        # default parameters for all agents
        for agent in self.agent_converter.all_psychsim_agents():
            agent.ps_agent.setHorizon(12)

    def add_distributors_recipes(self):

        ds1 = self.simulation.distributors[0]
        ds2 = self.simulation.distributors[1]
        available_recipes_1 = {
            ALLOCATION: [DistributorAllocateProportionalRecipe('alloc_prop')],
            # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
            # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
            ORDER_AMOUNT: [DistributorUpToOrderAmountRecipe('1ord_up')],
            ORDERING_SPLIT: [DistributorOrderSplitByTrustRecipe('split_trust')],
            TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
        }

        # creates state-action dynamics
        agent = self.agent_converter.get_psychsim_agent(ds1)
        agent.define_state_dynamics(available_recipes_1)

        available_recipes_2 = {
            ALLOCATION: [DistributorAllocateProportionalRecipe('alloc_prop')],
            # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
            # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
            ORDER_AMOUNT: [DistributorUpToOrderAmountRecipe('1ord_up')],
            ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_eq')],
            TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
        }

        # creates state-action dynamics
        agent = self.agent_converter.get_psychsim_agent(ds2)
        agent.define_state_dynamics(available_recipes_2)

    def add_manufacturers_recipes(self):

        allocate_equally = ManufacturerAllocateEquallyRecipe('alloc_eq')
        allocate_proportional = ManufacturerAllocateProportionalRecipe('alloc_prop')

        allocate_recipes_planning = [allocate_equally, allocate_proportional]
        allocate_recipes_no_planning = [allocate_equally]

        # gets distributors with planning capacity
        planning_mn = []
        for idx in self.mn_lookahead_idxs:
            planning_mn.append(self.simulation.manufacturers[idx])

        # adds corresponding recipes
        for mn in self.simulation.manufacturers:

            allocation_recipes = allocate_recipes_no_planning
            if mn in planning_mn:
                allocation_recipes = allocate_recipes_planning

        for mn in self.simulation.manufacturers:
            available_recipes = {
                ALLOCATION: allocation_recipes,
                # FORECAST_DEMAND: [ManufacturerDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [ManufacturerUpToLevelRecipe('calc_up')],
                PRODUCTION_AMOUNT: [ManufacturerUpToProductionRecipe('prod_pro')]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(mn)
            agent.define_state_dynamics(available_recipes)
