import random
import StringIO

from psychsim.agent import Agent
from psychsim.world import World
from agent_converter import AgentConverter

from psychsim.action import Action, ActionSet
from psychsim.pwl import *
from psychsim.probability import Distribution


class PsyAgent(Agent):
    def __init__(self, name, priority_act='None', previous_act='None'):
        super(PsyAgent, self).__init__(name)
        self.priority_act = priority_act
        self.previous_act = previous_act

    def decide(self, vector, horizon=None, others=None, model=None, selection=None, actions=None, keys=None):
        """
        Overwrite decide of psychsim/Agent class to include "priority" in selection
        """
        if model is None:
            model = self.world.getModel(self.name, vector)
        if selection is None:
            selection = self.getAttribute('selection', model)
        # What are my subjective beliefs for this decision?
        belief = self.getBelief(vector, model)

        # todo Pedro added state discretization
        num_groups = self.getAttribute('discretization', model)
        if num_groups is not None:
            for state in belief.domain():
                disc_state = self.discretize_state(state, num_groups)
                if str(disc_state) != str(state):
                    belief[disc_state] = belief[state]
                    del belief[state]

        # Do I have a policy telling me what to do?
        policy = self.getAttribute('policy', model)
        if policy:
            assert len(belief) == 1, 'Unable to apply PWL policies to uncertain beliefs'
            action = policy[belief.domain()[0]]
            if action:
                if isinstance(action, Action):
                    action = ActionSet([action])
                return {'action': action}
        if horizon is None:
            horizon = self.getAttribute('horizon', model)
        if actions is None:
            # Consider all legal actions (legality determined by my belief, circumscribed by real world)
            actions = self.getActions(vector)
            for state in belief.domain():
                actions = actions & self.getActions(state)
        if len(actions) == 0:
            # Someone made a boo-boo because there is no legal action for this agent right now
            buf = StringIO.StringIO()
            print >> buf, '%s has no legal actions in:' % (self.name)
            self.world.printVector(vector, buf)
            print >> buf, '\nwhen believing:'
            self.world.printState(belief, buf)
            msg = buf.getvalue()
            buf.close()
            raise RuntimeError, msg
        elif len(actions) == 1:
            # Only one possible action
            return {'action': iter(actions).next()}
        # Keep track of value function
        V = {}
        best = None
        for action in actions:
            # Compute value across possible worlds
            V[action] = {'__EV__': 0.}
            if isinstance(keys, dict):
                subkeys = keys[action]
            else:
                subkeys = keys
            for state in belief.domain():
                V[action][state] = self.value(state, action, horizon, others, model, subkeys)
                V[action]['__EV__'] += belief[state] * V[action][state]['V']
            if len(V[action]) > 1:
                # Determine whether this action is the best
                if best is None:
                    best = [action]
                elif V[action]['__EV__'] == V[best[0]]['__EV__']:
                    best.append(action)
                elif V[action]['__EV__'] > V[best[0]]['__EV__']:
                    best = [action]
        result = {'V*': V[best[0]]['__EV__'], 'V': V}
        # Make an action selection based on the value function
        if selection == 'distribution':
            values = {}
            for key, entry in V.items():
                values[key] = entry['__EV__']
            result['action'] = Distribution(values, self.getAttribute('rationality', model))
        elif selection == 'adhoc':
            # change action if current optimal action is X% better than previously selected action
            best_value = V[best[0]]['__EV__']
            act_found = False
            if self.previous_act != 'None':
                for act in actions:
                    if act['verb'] == self.previous_act:
                        prev_act = act
                        prev_value = V[prev_act]['__EV__']
                if prev_value == 0:
                    if prev_value <= best_value:
                        act_ratio = 0.5
                    else:
                        act_ratio = 0
                else:
                    act_ratio = (best_value-prev_value)/abs(prev_value)
                # print(act_ratio)
                if act_ratio > 0.9:
                    for act in best:
                        if self.priority_act in act['verb']:
                            # print('here 1')
                            result['action'] = act
                            act_found = True
                    if not act_found:
                        # print('here 2')
                        result['action'] = random.sample(best, 1)[0]
                else:
                    # print('here 3')
                    result['action'] = prev_act

            else:
                for act in best:
                    if self.priority_act in act['verb']:
                        # print('here 4')
                        result['action'] = act
                        act_found = True
                if not act_found:
                    # print('here 5')
                    result['action'] = random.sample(best, 1)[0]
        elif selection == 'adhoc_diff':
            # change action if current optimal action is X amount better than previously selected action
            best_value = V[best[0]]['__EV__']
            act_found = False
            if self.previous_act != 'None':
                for act in actions:
                    if act['verb'] == self.previous_act:
                        prev_act = act
                        prev_value = V[prev_act]['__EV__']

                act_diff = best_value-prev_value
                # print(act_ratio)
                if act_diff > 900:
                    for act in best:
                        if self.priority_act in act['verb']:
                            # print('here 1')
                            result['action'] = act
                            act_found = True
                    if not act_found:
                        # print('here 2')
                        result['action'] = random.sample(best, 1)[0]
                else:
                    # print('here 3')
                    result['action'] = prev_act

            else:
                for act in best:
                    if self.priority_act in act['verb']:
                        # print('here 4')
                        result['action'] = act
                        act_found = True
                if not act_found:
                    # print('here 5')
                    result['action'] = random.sample(best, 1)[0]

        elif selection == 'random':
            result['action'] = random.sample(best, 1)[0]
        elif selection == 'uniform':
            result['action'] = {}
            prob = 1. / float(len(best))
            for action in best:
                result['action'][action] = prob
            result['action'] = Distribution(result['action'])
        elif selection == 'priority':
            # if the priority action is among optimal actions, select this priority action
            act_found = False
            for act in best:
                if self.priority_act in act['verb']:
                    result['action'] = act
                    act_found = True
            if not act_found:
                result['action'] = random.sample(best, 1)[0]
        elif selection == 'previous':
            # if the previously selected action is among optimal actions, select this action
            act_found = False
            for act in best:
                if self.previous_act in act['verb']:
                    result['action'] = act
                    act_found = True
            if not act_found:
                result['action'] = random.sample(best, 1)[0]
        elif len(best) == 1:
            # If there is only one best action, all of the selection mechanisms devolve
            # to the same unique choice
            result['action'] = best[0]
        else:
            assert selection == 'consistent', 'Unknown action selection method: %s' % (selection)
            best.sort()
            result['action'] = best[0]
        return result


class PsychSimAgent(object):
    """
    PsychSimAgent is a base class for all the PsychSim agents.
    It creates a connection between the simulator and PsychSim agents.
    """

    def __init__(self, world, agent_converter, name="Agent"):
        """
        Creates a PsychSim agent with the given name
        :param World world: the PsychSim world to which attach the agent.
        :param AgentConverter agent_converter: the correspondence list between PsychSim agent ids and simulation agents.
        :param str name: the name of the agent to be created in PsychSim.
        """
        self.world = world
        self.agent_converter = agent_converter
        self.name = name

        # creates PsychSim agent
        # self.ps_agent = Agent(name)
        self.ps_agent = PsyAgent(name)

        # note: by adding ps_agent to self.world, the world of ps_agent will also be self.world
        self.world.addAgent(self.ps_agent)

        # common parameters
        self.smoothing_constant = 0.8
        self.totals = {}

    def create_state_variables(self):
        """
        Each agent has to create its own state variables.
        """
        pass

    def define_dependencies(self):
        """
        Each agent has to define the dependencies between the state variables.
        """
        pass

    def define_state_dynamics(self, available_recipes):
        """
        Each agent has to define the state variable dynamics according to its own recipes.
        :param dict available_recipes: a dictionary containing the different types of recipes available for the agent.
        """
        pass

    def update_psychsim_from_simulation(self, now):
        """
        Updates the agent's PsychSim state according to the simulation agent state.
        """
        pass

    def update_simulation_from_psychsim(self, outcomes):
        """
        Updates the simulation agent decisions according to the PsychSim agent's actions.
        """
        pass

    def __str__(self):
        return self.ps_agent.name

    def __hash__(self):
        return self.ps_agent.name.__hash__()

    @staticmethod
    def get_all_recipe_combinations(available_recipes):
        """
        Utility method that creates all possible combinations between the given recipes.
        :param dict available_recipes: a dictionary containing pairs of recipe type (str) - list of available recipes.
        :return: a list containing dictionaries of pairs recipe type (str) - recipe, each corresponding to a different
        combination of available recipes.
        :rtype list
        """
        all_recipe_combs = [{}]
        for key, recipes in available_recipes.iteritems():
            new_recipe_combs = []
            for recipe_comb in all_recipe_combs:
                for recipe in recipes:
                    new_recipe_comb = dict(recipe_comb)
                    new_recipe_comb[key] = recipe
                    new_recipe_combs.append(new_recipe_comb)
            all_recipe_combs = new_recipe_combs
        return all_recipe_combs
