from psychsim_agents.constants import ORDER_HI
from psychsim_agents.psychsim_health_center import PSHealthCenterAgent
from psychsim_agents.psychsim_manufacturer import PSManufacturerAgent
from recipe import Recipe
from helper_functions import *
from psychsim.action import Action


class GeneratePatients(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_cleaning_agent.PSCleaningAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        pass


class UpdatingReceivedDocks(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_cleaning_agent.PSCleaningAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        # "agent" is cleaning agent and "other" in the following lines are other agents
        # "agent" received_dock is dictionary of src and dst agent as key and arrays of their in-transit inventory as value
        # "other" received_dock is dictionary of src as key and the value of this periods received amount
        for other in agent.other_agents:
            # if other is PSManufacturerAgent:
            if isinstance(other, PSManufacturerAgent):
                # update received dock list
                for t in range(0, len(agent.received_dock[other]) - 1):
                    tree = makeTree(
                        setToFeatureMatrix(agent.received_dock[other][t], agent.received_dock[other][t + 1]))
                    agent.world.setDynamics(agent.received_dock[other][t], action, tree)
                # put production amount at last element of received dock
                tree = makeTree(
                    setToFeatureMatrix(agent.received_dock[other][len(agent.received_dock[other]) - 1],
                                       other.production_amount))
                agent.world.setDynamics(agent.received_dock[other][len(agent.received_dock[other]) - 1], action,
                                        tree)
                # update current production dock of manufacturer
                tree = makeTree(setToFeatureMatrix(other.production_dock, agent.received_dock[other][0]))
                agent.world.setDynamics(other.production_dock, action, tree)
            else:

                max = other.totals[ORDER_HI]
                num_samples = max / 10
                samples = get_bivariate_samples(div, 0, max, 0, max, num_samples, num_samples)

                for u in other.up_stream_agents:
                    if len(agent.received_dock[other, u]) == 1:
                        # put allocated amount to agent's received dock
                        tree = makeTree(
                            setToFeatureMatrix(agent.received_dock[other, u][0], u.allocate_to[other]))
                        agent.world.setDynamics(agent.received_dock[other, u][0], action, tree)
                        # update current received dock of agt
                        tree = makeTree(setToFeatureMatrix(other.received_dock[u], agent.received_dock[other, u][0]))
                        agent.world.setDynamics(other.received_dock[u], action, tree)

                    else:
                        for t in range(0, len(agent.received_dock[other, u]) - 1):
                            # shift agent's received dock
                            tree = makeTree(
                                setToFeatureMatrix(agent.received_dock[other, u][t],
                                                   agent.received_dock[other, u][t + 1]))
                            agent.world.setDynamics(agent.received_dock[other, u][t], action, tree)
                        # put allocated amount to agent's received dock's last element
                        tree = makeTree(
                            setToFeatureMatrix(agent.received_dock[other, u][len(agent.received_dock[other, u]) - 1],
                                               u.allocate_to[other]))
                        agent.world.setDynamics(
                            agent.received_dock[other, u][len(agent.received_dock[other, u]) - 1], action, tree)
                        # update current received dock of agt
                        tree = makeTree(setToFeatureMatrix(other.received_dock[u],
                                                           agent.received_dock[other, u][0]))
                        agent.world.setDynamics(other.received_dock[u], action, tree)

                    # expected received updates
                    if len(agent.expctd_recieved_dock[other, u]) == 1:
                        # put ordered amount to agent's expected received
                        tree = makeTree(multiSetMatrix(agent.residual[other, u],
                                                       {agent.expctd_recieved_dock[other, u][0]: 1.0,
                                                        agent.received_dock[other, u]: -1.0}))
                        agent.world.setDynamics(agent.residual[other, u], action, tree)

                        tree = makeTree(
                            multiSetMatrix(agent.expctd_recieved_dock[other, u][0], {agent.residual[other, u]: 1.0,
                                                                                     other.order_to[u]: 1.0}))
                        agent.world.setDynamics(agent.expctd_recieved_dock[other, u][0], action, tree)
                        # update current expected received of agt
                        tree = makeTree(setToFeatureMatrix(other.expctd_recieved[u],
                                                           agent.expctd_recieved_dock[other, u][0]))
                        agent.world.setDynamics(other.expctd_recieved[u], action, tree)
                    else:
                        tree = makeTree(multiSetMatrix(agent.residual[other, u],
                                                       {agent.expctd_recieved_dock[other, u][0]: 1.0,
                                                        agent.received_dock[other, u][0]: -1.0}))
                        agent.world.setDynamics(agent.residual[other, u], action, tree)
                        tree = makeTree(multiSetMatrix(agent.expctd_recieved_dock[other, u][0],
                                                       {agent.residual[other, u]: 1.0,
                                                        agent.expctd_recieved_dock[other, u][1]: 1.0}))
                        agent.world.setDynamics(agent.expctd_recieved_dock[other, u][0], action, tree)

                        for t in range(1, len(agent.expctd_recieved_dock[other, u]) - 1):
                            # shift agent's expected received
                            tree = makeTree(
                                setToFeatureMatrix(agent.expctd_recieved_dock[other, u][t],
                                                   agent.expctd_recieved_dock[other, u][t + 1]))
                            agent.world.setDynamics(agent.expctd_recieved_dock[other, u][t], action, tree)
                        # put ordered amount to agent's expected received's last element
                        tree = makeTree(
                            setToFeatureMatrix(agent.expctd_recieved_dock[other, u]
                                               [len(agent.expctd_recieved_dock[other, u]) - 1], other.order_to[u]))
                        agent.world.setDynamics(agent.expctd_recieved_dock[other, u]
                                                [len(agent.expctd_recieved_dock[other, u]) - 1], action, tree)
                        # update current received dock of agt
                        tree = makeTree(setToFeatureMatrix(other.expctd_recieved[u],
                                                           agent.expctd_recieved_dock[other, u][0]))
                        agent.world.setDynamics(other.expctd_recieved[u], action, tree)

                    # delivery rate updates
                    tree = makeTree({'if': thresholdRow(other.expctd_recieved[u], 0),
                                     True: tree_from_bivariate_samples(other.ontime_deliv[u],
                                                                       other.received_dock[u], other.expctd_recieved[u],
                                                                       samples, 0, num_samples - 1, 0, num_samples - 1),
                                     False: setToConstantMatrix(other.ontime_deliv[u], 1)})

                    agent.world.setDynamics(other.ontime_deliv[u], action, tree)


class UpdateOtherFeatures(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_cleaning_agent.PSCleaningAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        for other in agent.other_agents:
            tree = makeTree(setToFeatureMatrix(other.total_inventory, other.inv_after_allocation))
            agent.world.setDynamics(other.total_inventory, action, tree)
            if isinstance(other, PSHealthCenterAgent):
                tree = makeTree(setToFeatureMatrix(other.backlog, other.backlog_after_allocation))
                agent.world.setDynamics(other.backlog, action, tree)
            # if other is not PSHealthCenterAgent:
            if not isinstance(other, PSHealthCenterAgent):
                current_dem_dict = {}
                for d in other.down_stream_agents:
                    tree = makeTree(
                        multiSetMatrix(other.backlog_dict[d],
                                       {other.backlog_after_allocation[d]: 1.0, d.order_to[other]: 1.0}))
                    agent.world.setDynamics(other.backlog_dict[d], action, tree)
                    current_dem_dict[d.order_to[other]] = 1.0
                tree = makeTree(multiSetMatrix(other.current_demand, current_dem_dict))
                agent.world.setDynamics(other.current_demand, action, tree)
                tree = makeTree(multiSetMatrix(other.total_backlog,
                                               {other.total_backlog_after_allocation: 1.0, other.current_demand: 1.0}))
                agent.world.setDynamics(other.total_backlog, action, tree)
            # if other is not PSManufacturerAgent:
            if not isinstance(other, PSManufacturerAgent):
                for u in other.up_stream_agents:
                    tree = makeTree(
                        multiSetMatrix(other.on_order_dict[u],
                                       {other.onorder_after_received[u]: 1.0, other.order_to[u]: 1.0}))
                    agent.world.setDynamics(other.on_order_dict[u], action, tree)

            if isinstance(other, PSManufacturerAgent):
                tree = makeTree(multiSetMatrix(other.total_in_production,
                                               {other.total_inproduction_after_received: 1.0,
                                                other.production_amount: 1.0}))
                agent.world.setDynamics(other.total_in_production, action, tree)
