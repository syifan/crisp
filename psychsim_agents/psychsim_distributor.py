from psychsim_agent import PsychSimAgent
from recipes_distributor import *
from simulator.decision import *
from psychsim.reward import *
from psychsim.world import World
from agent_converter import AgentConverter


class PSDistributorAgent(PsychSimAgent):
    """Defines the states and dynamics for the distributor agent"""

    def __init__(self, world, agent_converter, name):
        """
        Creates a PsychSim distributor agent with the given name and parameters.
        :param World world: the PsychSim world to which attach the agent.
        :param AgentConverter agent_converter: the correspondence list between PsychSim agent ids and simulation agents.
        :param str name: the name of the agent to be created in PsychSim.
        """
        super(PSDistributorAgent, self).__init__(world, agent_converter, name)
        self.down_stream_agents = []  # downstream agents of distributor
        self.up_stream_agents = []  # upstream agents of distributor
        self.num_up_stream = None
        self.num_down_stream = None

        self.total_inventory = None
        self.inv_after_received = None  # inventory after receiving shipment from upstream nodes
        self.inv_after_allocation = None  # inventory after allocating to patients

        # self.last_forecast = None  # last forecast demand
        # self.forecast_part_one = None  # intermediate variables
        # self.forecast_part_two = None  # intermediate variables
        # self.current_forecast = None  # current forecast demand
        # self.smoothing_coef = None  # used in forecasting
        self.current_demand = None  # current demand

        self.up_to_level = None
        # self.lead_time = None

        self.order_amount = None

        self.total_on_order = None  # total on-order from all upstream nodes
        self.on_order_dict = {}  # keys = upstream nodes, values = on-order from that upstream node
        self.onorder_after_received = {}  # on-order dictionary after receiving shipment from upstream nodes

        self.order_to = {}  # keys = upstream nodes, values = order amount to that upstream node
        self.ontime_deliv = {}  # keys = upstream nodes, values = on-time delivery rate of that upstream node
        self.trust = {}  # trustworthiness that distrbutor attributes to each of its upstreams (between 0 and 1)
        # self.coef_2 = {}  # intermediate variables used for calculating trust
        # self.final_coef = {}  # intermediate variables used for calculating trust
        # self.sum_coef = None  # intermediate variables used for calculating trust

        self.received_dock = {}  # keeps information about what agent has received (used by cleaning agent in forward
        # planning)
        self.expctd_recieved = {}  # amount of product that agent expects to receive from its upstream now, according

        # backlog includes current demand + back order for each agent
        self.backlog_dict = {}  # keys = downstream nodes, values = backlog amount to that downstream node
        self.backlog_after_allocation = {}  # backlog after allocating to downstream
        self.total_backlog = None
        self.total_backlog_after_allocation = None

        self.allocate_to = {}  # keys = downstream nodes, values = allocation amount to that downstream node
        self.temp_allocate_to = {}  # intermediate variables
        self.equal_allocate = None  # allocation amount in order equally recipe
        self.extra_allocate = None  # extra allocation in allocate equally recipe (leftover of the downstream that
        # doesn't need its share)

        # used in prefer one healthcenter recipe
        self.new_inv_1 = None  # intermediate variables
        self.new_inv_2 = None  # intermediate variables

        self.totals = {INV_HI: 400, UP_TO_HI: 400, UP_STR_HI: 3, DN_STR_HI: 3, ORDER_HI: 400, DEM_HI: 400,
                       BL_HI: 400, CUR_DEM_HI: 400, LEAD_TIME_HI: 3}  # upper bounds on variables' values

    def create_state_variables(self):
        """
        create PsychSim state variables
        Each agent has to create its own state variables.
        """
        sim_agent = self.agent_converter.get_sim_agent_from_psychsim_agent(self)
        agent_name = self.ps_agent.name

        # defines properties / parameters
        for u in sim_agent.upstream_nodes:
            self.up_stream_agents.append(self.agent_converter.get_psychsim_agent(u))
        for d in sim_agent.downstream_nodes:
            self.down_stream_agents.append(self.agent_converter.get_psychsim_agent(d))

        # syntactic sugar: add properties to agents from state key variables
        self.num_up_stream = self.world.defineState(agent_name, NUM_UP_STREAM, int, lo=0, hi=self.totals[UP_STR_HI])
        self.world.setFeature(self.num_up_stream, len(self.up_stream_agents))

        self.num_down_stream = self.world.defineState(agent_name, NUM_DOWN_STREAM, int, lo=0, hi=self.totals[DN_STR_HI])
        self.world.setFeature(self.num_down_stream, len(self.down_stream_agents))

        self.total_inventory = self.world.defineState(agent_name, INVENTORY, int, lo=0, hi=self.totals[INV_HI])
        self.world.setFeature(self.total_inventory, sim_agent.inventory_level())

        self.inv_after_received = self.world.defineState(
            self.name, INV_AFTER_RECEIVED, int, lo=0.0, hi=self.totals[INV_HI])
        self.world.setFeature(self.inv_after_received, 0.0)

        self.inv_after_allocation = self.world.defineState(
            self.name, INV_AFTER_ALLOCATION, int, lo=0.0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.inv_after_allocation, 0.0)

        self.current_demand = self.world.defineState(agent_name, CURRENT_DEMAND, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.current_demand, 0)

        # self.last_forecast = self.world.defineState(agent_name, LAST_FORECAST, int, lo=0, hi=self.totals[DEM_HI])
        # self.world.setFeature(self.last_forecast, sim_agent.last_forecast)
        #
        # self.forecast_part_one = self.world.defineState(agent_name, FORECAST_ONE, int, lo=0, hi=self.totals[CUR_DEM_HI])
        # self.world.setFeature(self.forecast_part_one, 0)
        #
        # self.forecast_part_two = self.world.defineState(agent_name, FORECAST_TWO, int, lo=0, hi=self.totals[CUR_DEM_HI])
        # self.world.setFeature(self.forecast_part_two, 0)
        #
        # self.current_forecast = self.world.defineState(agent_name, CURRENT_FORECAST, int, lo=0, hi=self.totals[DEM_HI])
        # self.world.setFeature(self.current_forecast, 0)
        #
        # self.smoothing_coef = self.world.defineState(agent_name, SMOOTHING_CONSTANT, float, lo=0, hi=1)
        # self.world.setFeature(self.smoothing_coef, self.smoothing_constant)

        self.up_to_level = self.world.defineState(agent_name, UP_TO_LEVEL, int, lo=0, hi=self.totals[UP_TO_HI])
        self.world.setFeature(self.up_to_level, 0)

        # self.lead_time = self.world.defineState(agent_name, LEAD_TIME, int, lo=0, hi=self.totals[LEAD_TIME_HI])
        # # todo this lead time is the one included in up-to level calculation, can be max or avg of all lead-times
        # self.world.setFeature(self.lead_time, 1)

        self.order_amount = self.world.defineState(agent_name, ORDER_AMOUNT, int, lo=0, hi=self.totals[ORDER_HI])
        self.world.setFeature(self.order_amount, 0)

        self.total_on_order = self.world.defineState(agent_name, TOTAL_ON_ORDER, int, lo=0, hi=self.totals[ORDER_HI])
        self.world.setFeature(self.total_on_order, sum(oo.amount for oo in sim_agent.on_order))

        for u in self.up_stream_agents:
            sim_upst = self.agent_converter.get_sim_agent_from_psychsim_agent(u)
            self.on_order_dict[u] = self.world.defineState(
                self.name, ON_ORDER + str(u), int, lo=0, hi=self.totals[ORDER_HI])
            total_agent_onorder = 0
            for oo in sim_agent.on_order:
                if oo.dst is sim_upst:
                    total_agent_onorder += oo.amount
            self.world.setFeature(self.on_order_dict[u], total_agent_onorder)

            self.onorder_after_received[u] = self.world.defineState(
                self.name, ONORDER_AFTER_RECEIVED + str(u), int, lo=0, hi=self.totals[ORDER_HI])
            self.world.setFeature(self.onorder_after_received[u], 0)

            self.order_to[u] = self.world.defineState(
                agent_name, ORDER_TO + str(u), int, lo=0, hi=self.totals[ORDER_HI])
            self.world.setFeature(self.order_to[u], 0)

            self.ontime_deliv[u] = self.world.defineState(agent_name, ON_TIME_DELIV + str(u), float, lo=0, hi=1)
            self.world.setFeature(self.ontime_deliv[u], 1)
            #
            self.trust[u] = self.world.defineState(agent_name, TRUST + str(u), float, lo=0,
                                                   hi=1)
            self.world.setFeature(self.trust[u], 1)
            #
            # self.coef_2[u] = self.world.defineState(
            #     agent_name, COEF_TWO + str(u), float, lo=0, hi=1)
            # self.world.setFeature(self.coef_2[u], 1)
            #
            # self.final_coef[u] = self.world.defineState(
            #     agent_name, FINAL_COEF + str(u), float, lo=0, hi=1)
            # self.world.setFeature(self.final_coef[u], 1)

            self.received_dock[u] = self.world.defineState(
                agent_name, RECEIVED_DOCK + str(u), int, lo=0, hi=self.totals[ORDER_HI])
            self.world.setFeature(self.received_dock[u], 0)

            self.expctd_recieved[u] = self.world.defineState(
                agent_name, EXPCTD_RECIEVED + str(u), int, lo=0, hi=self.totals[ORDER_HI])
            self.world.setFeature(self.expctd_recieved[u], 0)

            # define trust network
            # key = self.world.defineRelation(agent_name, u.ps_agent.name, 'trust', float, lo=0.0, hi=1.0)
            # self.world.setFeature(key, 1.0)
        self.sum_coef = self.world.defineState(agent_name, SUM_COEF, float, lo=0, hi=1)
        self.world.setFeature(self.sum_coef, 1)

        self.total_backlog = self.world.defineState(self.name, TOTAL_BACKLOG, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.total_backlog, sum(bl.amount for bl in sim_agent.backlog))

        self.total_backlog_after_allocation = self.world.defineState(
            self.name, TOTAL_BACKLOG_AFTER_ALLOCATION, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.total_backlog_after_allocation, 0)

        for d in self.down_stream_agents:
            self.backlog_dict[d] = self.world.defineState(
                self.name, BACKLOG + str(d), int, lo=0, hi=self.totals[DEM_HI])
            total_agent_demand = 0
            sim_dnst = self.agent_converter.get_sim_agent_from_psychsim_agent(d)
            for bl in sim_agent.backlog:
                if bl.src is sim_dnst:
                    total_agent_demand += bl.amount
            self.world.setFeature(self.backlog_dict[d], total_agent_demand)

            self.backlog_after_allocation[d] = self.world.defineState(
                self.name, BACKLOG_AFTER_ALLOCATION + str(d), int, lo=0, hi=self.totals[DEM_HI])
            self.world.setFeature(self.backlog_after_allocation[d], 0)

            self.allocate_to[d] = self.world.defineState(
                agent_name, ALLOCATE_TO + str(d), int, lo=0, hi=self.totals[INV_HI])
            self.world.setFeature(self.allocate_to[d], 0)

            self.temp_allocate_to[d] = self.world.defineState(
                agent_name, TEMP_ALLOCATE_TO + str(d), int, lo=0, hi=self.totals[INV_HI])
            self.world.setFeature(self.temp_allocate_to[d], 0)

            # self.temp_allocate_initial[c] = self.world.defineState(
            #   agent_name, TEMP_ALLOCATE_INITIAL + c, int, lo=0, hi=self.totals[INV_HI])
            # self.world.setFeature(TEMP_ALLOCATE_INITIAL + c, 0)

        self.equal_allocate = self.world.defineState(self.name, 'equal-allocate', int, lo=0, hi=self.totals[INV_HI])
        self.extra_allocate = self.world.defineState(self.name, 'extra-allocate', int, lo=0, hi=self.totals[INV_HI])

        self.new_inv_1 = self.world.defineState(self.name, NEW_INV_1, int, lo=0, hi=self.totals[INV_HI])
        self.world.setFeature(self.new_inv_1, 0)
        self.new_inv_2 = self.world.defineState(self.name, NEW_INV_2, int, lo=0, hi=self.totals[INV_HI])
        self.world.setFeature(self.new_inv_2, 0)

    def define_dependencies(self):
        """
        Each agent has to define the dependencies between the state variables.
        Whenever is calculated based on another variable there need to be a dependecy defined for the pair.
        """
        # dependency for Distributor Receive Production
        for u in self.up_stream_agents:
            self.world.addDependency(self.inv_after_received, self.received_dock[u])
        self.world.addDependency(self.inv_after_received, self.total_inventory)

        for u in self.up_stream_agents:
            self.world.addDependency(self.onorder_after_received[u], self.received_dock[u])
            self.world.addDependency(self.onorder_after_received[u], self.on_order_dict[u])

        self.world.addDependency(self.equal_allocate, self.num_down_stream)
        self.world.addDependency(self.equal_allocate, self.inv_after_received)
        self.world.addDependency(self.extra_allocate, self.equal_allocate)

        # dependencies for allocation recipes
        for d in self.down_stream_agents:
            # self.world.addDependency(self.total_backlog, self.backlog_dict[d])
            # self.world.addDependency(self.temp_allocate_initial[d], self.backlog_dict[d])
            # self.world.addDependency(self.temp_allocate_initial[d], self.total_backlog)
            # self.world.addDependency(self.temp_allocate_to[d], self.inv_after_received)
            # self.world.addDependency(self.temp_allocate_to[d], self.temp_allocate_initial[d])
            # self.world.addDependency(self.temp_allocate_to[d], self.num_down_stream)
            # self.world.addDependency(self.allocate_to[d], self.temp_allocate_to[d])
            # self.world.addDependency(self.allocate_to[d], self.backlog_dict[d])
            self.world.addDependency(self.total_backlog, self.backlog_dict[d])
            self.world.addDependency(self.temp_allocate_to[d], self.equal_allocate)
            self.world.addDependency(self.temp_allocate_to[d], self.backlog_dict[d])
            self.world.addDependency(self.extra_allocate, self.temp_allocate_to[d])
            self.world.addDependency(self.allocate_to[d], self.temp_allocate_to[d])
            self.world.addDependency(self.allocate_to[d], self.backlog_dict[d])
            self.world.addDependency(self.allocate_to[d], self.extra_allocate)
        # dependencies for priority based allocation
        h1 = self.down_stream_agents[0]
        h2 = self.down_stream_agents[1]
        self.world.addDependency(self.allocate_to[h1], self.backlog_dict[h1])
        self.world.addDependency(self.allocate_to[h1], self.inv_after_received)
        self.world.addDependency(self.new_inv_1, self.inv_after_received)
        self.world.addDependency(self.new_inv_1, self.allocate_to[h1])
        self.world.addDependency(self.allocate_to[h2], self.new_inv_1)
        self.world.addDependency(self.allocate_to[h2], self.backlog_dict[h2])

        self.world.addDependency(self.allocate_to[h2], self.backlog_dict[h2])
        self.world.addDependency(self.allocate_to[h2], self.inv_after_received)
        self.world.addDependency(self.new_inv_2, self.allocate_to[h2])
        self.world.addDependency(self.new_inv_2, self.inv_after_received)
        self.world.addDependency(self.allocate_to[h1], self.new_inv_2)
        self.world.addDependency(self.allocate_to[h1], self.backlog_dict[h1])

        # dependencies for intermediate state update
        self.world.addDependency(self.inv_after_allocation, self.inv_after_received)
        for d in self.down_stream_agents:
            self.world.addDependency(self.inv_after_allocation, self.allocate_to[d])

        for d in self.down_stream_agents:
            self.world.addDependency(self.backlog_after_allocation[d], self.allocate_to[d])
            self.world.addDependency(self.backlog_after_allocation[d], self.backlog_dict[d])

        # dependencies for demand forecast
        # self.world.addDependency(self.forecast_part_one, self.current_demand)
        # self.world.addDependency(self.forecast_part_one, self.smoothing_coef)
        # self.world.addDependency(self.forecast_part_two, self.last_forecast)
        # self.world.addDependency(self.forecast_part_two, self.smoothing_coef)
        # self.world.addDependency(self.current_forecast, self.forecast_part_one)
        # self.world.addDependency(self.current_forecast, self.forecast_part_two)
        #
        # # dependencies for up-to level
        # self.world.addDependency(self.up_to_level, self.current_forecast)
        # self.world.addDependency(self.up_to_level, self.lead_time)

        # dependencies for orders
        for d in self.down_stream_agents:
            self.world.addDependency(self.total_backlog_after_allocation,
                                     self.backlog_after_allocation[d])
        for u in self.up_stream_agents:
            self.world.addDependency(self.total_on_order, self.onorder_after_received[u])
        self.world.addDependency(self.order_amount, self.up_to_level)
        self.world.addDependency(self.order_amount, self.inv_after_allocation)
        self.world.addDependency(self.order_amount, self.total_backlog_after_allocation)
        self.world.addDependency(self.order_amount, self.total_on_order)

        for u in self.up_stream_agents:
            # self.world.addDependency(self.coef_2[u], self.trust[u])
            # self.world.addDependency(self.sum_coef, self.coef_2[u])
            # self.world.addDependency(self.final_coef[u], self.trust[u])
            # self.world.addDependency(self.final_coef[u], self.sum_coef)
            # self.world.addDependency(self.order_to[u], self.final_coef[u])
            self.world.addDependency(self.order_to[u], self.order_amount)
            self.world.addDependency(self.order_to[u], self.num_up_stream)

        # dependencies for trust update
        # for u in self.up_stream_agents:
        #     self.world.addDependency(self.trust[u], self.ontime_deliv[u])

    def define_state_dynamics(self, available_recipes):
        """
        Each agent has to define the state variable dynamics according to its own recipes.
        :param dict available_recipes: a dictionary containing the different types of recipes available for the agent.
        """
        update_received_inventory = DistributorReceiveShipmentRecipe('rcv_ship')
        intermediate_state_update = DistributorIntermediateStateUpdateRecipe('int_upd')

        # define the recipes through combination of trust, order, allocation, etc
        recipe_combinations = PsychSimAgent.get_all_recipe_combinations(available_recipes)
        for recipe_comb in recipe_combinations:

            # creates and adds action with name from all recipes in the combination
            action_name = "-".join([recipe.name for recipe in recipe_comb.values()])
            action = self.ps_agent.addAction({'verb': action_name})

            # add trust recipe
            if recipe_comb.has_key(TRUST):
                recipe_comb[TRUST].register_recipe(self, action)

            # add received inventory
            update_received_inventory.register_recipe(self, action)

            # add allocation recipe
            if recipe_comb.has_key(ALLOCATION):
                recipe_comb[ALLOCATION].register_recipe(self, action)

            # intermediate update of total inventory using dummy variable
            intermediate_state_update.register_recipe(self, action)

            # add forecasting demand
            if recipe_comb.has_key(FORECAST_DEMAND):
                recipe_comb[FORECAST_DEMAND].register_recipe(self, action)

            # add up-to-level calculation
            if recipe_comb.has_key(CALCULATE_UP_TO_LEVEL):
                recipe_comb[CALCULATE_UP_TO_LEVEL].register_recipe(self, action)

            # add ordering amount recipe
            if recipe_comb.has_key(ORDER_AMOUNT):
                recipe_comb[ORDER_AMOUNT].register_recipe(self, action)

            # add ordering split recipe
            if recipe_comb.has_key(ORDERING_SPLIT):
                recipe_comb[ORDERING_SPLIT].register_recipe(self, action)

    def update_psychsim_from_simulation(self, now):
        """
        Updates the agent's PsychSim state according to the simulation agent state.
        """
        sim_agent = self.agent_converter.get_sim_agent_from_psychsim_agent(self)

        # include if the network changes during the simulation time
        # self.downStreamNames = []
        # self.upStreamNames = []
        # for s in sim_agent.upstream_nodes:
        #     self.upStreamNames.append('MN_' + str(s.id))
        # for c in sim_agent.downstream_nodes:
        #     self.downStreamNames.append('HC_' + str(c.id))
        # self.world.setFeature(NUM_UP_STREAM, len(self.upStreamNames))
        # self.world.setFeature(NUM_DOWN_STREAM, len(self.downStreamNames))
        self.world.setFeature(self.total_inventory, sim_agent.inventory_level())
        self.world.setFeature(self.inv_after_received, 0.0)
        self.world.setFeature(self.inv_after_allocation, 0.0)

        self.world.setFeature(self.current_demand, sim_agent.demand(now))
        # self.world.setFeature(self.last_forecast, sim_agent.last_forecast)
        # self.world.setFeature(self.forecast_part_one, 0)
        # self.world.setFeature(self.forecast_part_two, 0)
        # self.world.setFeature(self.current_forecast, 0)
        # self.world.setFeature(self.smoothing_coef, self.smoothing_constant)

        self.world.setFeature(self.up_to_level, sim_agent.up_to_level)
        # todo this lead time is the one included in up-to level calculation, can be max or avg of all lead-times
        # self.world.setFeature(self.lead_time, 1)
        self.world.setFeature(self.order_amount, 0)

        self.world.setFeature(self.total_on_order, sum(oo.amount for oo in sim_agent.on_order))
        for u in self.up_stream_agents:
            sim_upst = self.agent_converter.get_sim_agent_from_psychsim_agent(u)
            total_agent_onorder = 0
            for oo in sim_agent.on_order:
                if oo.dst is sim_upst:
                    total_agent_onorder += oo.amount
            self.world.setFeature(self.on_order_dict[u], total_agent_onorder)
            self.world.setFeature(self.onorder_after_received[u], 0)
            self.world.setFeature(self.order_to[u], 0)

            self.world.setFeature(self.received_dock[u], 0)
            self.world.setFeature(self.expctd_recieved[u], 0)
            deliv_rate = sim_agent.ontime_deliv_rate[sim_upst]
            self.world.setFeature(self.ontime_deliv[u], deliv_rate)
            # todo is this necessary?
            self.world.setFeature(self.trust[u], deliv_rate)

        self.world.setFeature(self.total_backlog, sum(bl.amount for bl in sim_agent.backlog))
        self.world.setFeature(self.total_backlog_after_allocation, 0)

        for d in self.down_stream_agents:
            total_agent_demand = 0
            sim_dnst = self.agent_converter.get_sim_agent_from_psychsim_agent(d)
            for bl in sim_agent.backlog:
                if bl.src is sim_dnst:
                    total_agent_demand += bl.amount
            self.world.setFeature(self.backlog_dict[d], total_agent_demand)
            self.world.setFeature(self.backlog_after_allocation[d], 0)
            self.world.setFeature(self.allocate_to[d], 0)
            self.world.setFeature(self.temp_allocate_to[d], 0)
            # self.world.setFeature(TEMP_ALLOCATE_INITIAL + c, 0)

    def update_simulation_from_psychsim(self, outcomes):
        """
        Updates the simulation agent decisions according to the PsychSim agent's actions.
        """

        sim_agent = self.agent_converter.get_sim_agent_from_psychsim_agent(self)

        # -- order decision
        for u, amt in self.order_to.iteritems():
            decision_o = OrderDecision()
            decision_o.upstream = self.agent_converter.get_sim_agent_from_psychsim_agent(u)
            # decision_o.amount = int(round(outcomes[amt]))
            decision_o.amount = outcomes[amt]
            sim_agent.decisions.append(decision_o)

        # -- allocation decision
        all_dict = {}
        for d, amt in self.allocate_to.iteritems():
            # all_dict[self.psychsim_to_simulation[dnst]] = int(round(outcomes[amt]))
            dnst_sim = self.agent_converter.get_sim_agent_from_psychsim_agent(d)
            # print dnst_sim.id
            all_dict[dnst_sim] = outcomes[amt]
            # print all_dict[dnst_sim]

        if len(sim_agent.inventory) >= 1:
            inv_ptr = 0
            inv_left = sim_agent.inventory[0].amount
            for bl in sim_agent.backlog:
                bl_left = bl.amount
                while (all_dict[bl.src] > 0 and bl_left > 0):
                    if min(all_dict[bl.src], bl_left) <= inv_left:
                        decision_al = AllocateDecision()
                        decision_al.amount = min(all_dict[bl.src], bl_left)
                        all_dict[bl.src] -= decision_al.amount
                        bl_left -= decision_al.amount
                        inv_left -= decision_al.amount
                        decision_al.item = sim_agent.inventory[inv_ptr]
                        decision_al.order = bl
                        sim_agent.decisions.append(decision_al)
                    else:
                        if inv_left > 0:
                            decision_al = AllocateDecision()
                            decision_al.amount = inv_left
                            all_dict[bl.src] -= decision_al.amount
                            bl_left -= decision_al.amount
                            decision_al.item = sim_agent.inventory[inv_ptr]
                            decision_al.order = bl
                            sim_agent.decisions.append(decision_al)
                        inv_ptr += 1
                        if inv_ptr < len(sim_agent.inventory):
                            inv_left = sim_agent.inventory[inv_ptr].amount
                        else:
                            return
