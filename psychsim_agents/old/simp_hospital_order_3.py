
'''
Psychsim web site https://github.com/pynadath/psychsim.git
Documentation in doc directory
'''

import sys
from ConfigParser import SafeConfigParser
from argparse import ArgumentParser
import StringIO

from psychsim.pwl import *
from psychsim.reward import *
from psychsim.action import *
from psychsim.world import World,stateKey,actionKey,binaryKey,modelKey,makeFuture
from psychsim.agent import Agent


    # Create scenario
class Order:

    def __init__(self,turnOrder):

        self.maxRounds=4
        self.world = World()
        #SafetyStock = 1
        numHospitals = 2
        numSupplies = 2
        hospital1 = Agent('H1')
        hospital2 = Agent('H2')
        
        supplier1 = Agent('S1')
        supplier2 = Agent('S2')
        hospAgts = [hospital1, hospital2]
        # STACY ADDED
        suppAgtNames = {'H1':['S1','S2'],'H2':['S1','S2']}
        totals = {'hInv':30,'hUpTo':30,'hOnOrder':30, 'hUpStr':10, 'hOrderAmt':50}


        # this is how you might define phases if you want to do that
        # world.defineState(None,'phase',list,['order','allocate'],
        #                   description='The current stage of the agent decision-making')
        # world.setState(None,'phase','allocate')
        
        # define Hospital agents
        # Player state, actions and parameters common to all Hospital Agents

        
        # define Hospital agents
        # Player state, actions and parameters common to all Hospital Agents
        for i in range(numHospitals):
            me = hospAgts[i]
            self.world.addAgent(me)
            # Define the State
            # many of these would be linked to states in the  simulation
            self.world.defineState(me.name,'TotalInventory',int,lo=0,hi=totals['hInv'])
            me.setState('TotalInventory',5.0)
            self.world.defineState(me.name,'UpToLevel',int,lo=0,hi=totals['hUpTo'])
            me.setState('UpToLevel',21)
            self.world.defineState(me.name,'TotalOnOrder',int,lo=0,hi=totals['hOnOrder'])
            me.setState('TotalOnOrder',4)
            self.world.defineState(me.name,'NumUpStream',int,lo=0,hi=totals['hUpStr'])
            me.setState('NumUpStream',2)
            self.world.defineState(me.name,'AdjNumUpStream',float,lo=0,hi=totals['hUpStr'])
            me.setState('AdjNumUpStream',2.0)
            self.world.defineState(me.name, 'OrderAmount', int, lo=0, hi=totals['hOrderAmt'])
            me.setState('OrderAmount',0)
            for j in range(0, 2):
                self.world.defineState(me.name, 'OrderTo'+str(j), int, lo=0, hi=totals['hOrderAmt'])
                me.setState('OrderTo'+str(j), 0)

            # These dependencies tell PyschSim the order in which to compute variables, for example
            # Compute new value for OrderTo only *after* computing new value for order amount
            #

            self.world.addDependency(stateKey(me.name, 'OrderAmount'), stateKey(me.name, 'TotalInventory'))
            self.world.addDependency(stateKey(me.name, 'OrderAmount'), stateKey(me.name, 'UpToLevel'))
            self.world.addDependency(stateKey(me.name, 'OrderAmount'), stateKey(me.name, 'TotalOnOrder'))

            for j in range(0, 2):
                self.world.addDependency(stateKey(me.name, 'OrderTo'+str(j)), stateKey(me.name, 'OrderAmount'))
                # STACY ADDED
                self.world.addDependency(stateKey(me.name, 'OrderTo'+str(j)), stateKey(me.name, 'AdjNumUpStream'))
            
            # Recipes --- which in PsychSim we can treat as Actions

            #STACY ADDED
            if me.name == 'H1':
                order2 = me.addAction({'verb': 'order','recipe':'split_order_total_offset_by_trust'})
            else:
                order1 = me.addAction({'verb': 'order','recipe':'split_order_equally'})

            # me.setLegal(order1,makeTree({'if': equalRow(stateKey(None, 'phase'),'order'),
            #                              True:  True,     # Orders are legal in the order phase                                           
            #                              False: False})) # Orders are illegal in all other phases
            #          ET CETERA

            # STACY ADDED
            # Possibilities for maintaining network
            # we need to know:
            #    who the suppliers are
            #    which are active
            #    how many are active to derive how to split the order equally
            #    then split the order across the active suppliers
            # To determine how many are active we could
            #    1. could maintain a statekey that is the number of active suppliers
            #       which is decremented or incremented when a supplier changes state
            #    2. calculate (sum-up) the # of active supplies on the fly
            # Then what is the ordering action
            #    Iis it one action that is used for all suppliers
            #    Or multiple actions one per supplier
            #    And then how does the specific supplier get listed
            # Or is it not an action at all but rather a belief update
            #    and whose belief gets updated mine or the suppliers

            # First lets define the hospital suppler relation...
            # it could be booleans such as
            #   self.world.defineRelation('H1','S1','supplier', bool)
            #   self.world.defineRelation('H1','S1','active_supplier', bool)
            # But instead lets make it a float that factors in trust which is operationalized as an expectation that a supplier
            # will provide some percentage of a full order
            #   self.world.defineRelation('H1','S1','trust', float, lo=0.0, hi=1.0)

            for supplier in suppAgtNames[me.name]:
                key = self.world.defineRelation(me.name,supplier,'trust', float, lo=0.0, hi=1.0)
                self.world.setFeature(key,1.0)

            # To access this value and use it in a dynamics function
            # (NOTE this is nonsensical - just an example of how to use it
            # let atom = some action
            # trustkey = binaryKey('H1','SI','trust')
            # tree = makeTree({'if':thresholdRow(trustkey,.5),
            #                  True: incrementMatrix(key,0.1,)
            #                  False:setToConstantMatrix(key, 0)})
            # world.setDynamics(key,atom,tree)



            # Parameters
            # Horizon is how far aead agent reasons to determine next action
            me.setHorizon(1)
            # Discount is how much agent discounts future rewards
            me.setAttribute('discount',0.9)

        # no reward function has been set

        #  turnorder is the order in which agents make decisions
        # in this example turnOrder is set below when the class is instantiated

        self.world.setOrder(turnOrder)

        # to show the impact lets make H1 not trust S1
        self.world.setFeature(binaryKey('H1','S1','trust'), .7)
         
        # Sets the termination condition when running the simulation
        # self.world.addTermination(makeTree({'if': thresholdRow(stateKey(None,'round'),self.maxRounds),
        #                           True: True, False: False}))

        # Dynamics of hospital orders
        for h in hospAgts:
            TotalInventory = stateKey(h.name,'TotalInventory')
            UpToLevel = stateKey(h.name,'UpToLevel')
            TotalOnOrder = stateKey(h.name,'TotalOnOrder')
            NumUpStream = stateKey(h.name,'NumUpStream')
            AdjNumUpStream = stateKey(h.name,'AdjNumUpStream')
            OrderAmount = stateKey(h.name,'OrderAmount')

            OrderTo=[]
            for u in range(0,2):
                OrderTo.append(stateKey(h.name,'OrderTo'+str(u)))

            #recipe1: split_order_equally
            atom = Action({'subject': h.name,'verb': 'order','recipe': 'split_order_equally'})
            tree = makeTree({'if': multiCompareRow({UpToLevel:1.0,TotalInventory:-1.0,TotalOnOrder:-1.0}),
                             False:setToConstantMatrix(OrderAmount, 0),
                             True:multiSetMatrix(OrderAmount, {UpToLevel:1.0,TotalInventory:-1.0,TotalOnOrder:-1.0})})
            self.world.setDynamics(OrderAmount,atom,tree)

            for i in range(0, 2):
                tree=makeTree({'if':thresholdRow(OrderAmount,0),
                               True:setToScaledKeyMatrix(OrderTo[i],OrderAmount,1.0,NumUpStream,.1),
                               False:setToConstantMatrix(OrderTo[i], 0)})
                self.world.setDynamics(OrderTo[i], atom, tree)

            ############################
            # STACY ADDED

            #recipe2: split_order_total_offset_by_trust
            # first figure out order amount
            atom = Action({'subject': h.name,'verb': 'order','recipe': 'split_order_total_offset_by_trust'})
            tree = makeTree({'if': multiCompareRow({UpToLevel:1.0,TotalInventory:-1.0,TotalOnOrder:-1.0}),
                             False:setToConstantMatrix(OrderAmount, 0),
                             True:multiSetMatrix(OrderAmount, {UpToLevel:1.0,TotalInventory:-1.0,TotalOnOrder:-1.0})})
            self.world.setDynamics(OrderAmount,atom,tree)

            # sum up the suppliers using their trust value as a kind of weird way to estimate likelihood of delivery
            # note unlike above recipe of split equall this handles the case of suppliers not being active
            SuppDict ={}
            for s in suppAgtNames[h.name]:
                SuppDict[binaryKey(h.name, s,"trust")] = 1.0
            tree = makeTree(multiSetMatrix(AdjNumUpStream,SuppDict))
            self.world.setDynamics(AdjNumUpStream,atom,tree)
            
            # so now order but the divisor is the summed trust values (so you end up ordering more than OrderAmount
            # and entirely untrusted suppliers get no order at all
            
            for i in range(0, 2): # this really should loop over supplier names and do so regardless if they are active
                s = suppAgtNames[h.name][i]
                print s
                tree=makeTree({'if':thresholdRow(OrderAmount,0),
                               True: {'if':thresholdRow(binaryKey(h.name,s,"trust"), 0.01), # is this supplier active/trusted at all
                                      True: setToScaledKeyMatrix(OrderTo[i],OrderAmount,1.0,AdjNumUpStream,.1),
                                      False:setToConstantMatrix(OrderTo[i], 0)},
                               False:setToConstantMatrix(OrderTo[i], 0)})
                # instead of Order{i} we probably should directly manipulate beliefs of Supplier (see comment above)
                self.world.setDynamics(OrderTo[i], atom, tree) 

            # STACY ENDED
            ############################

    def runit(self,Msg):

        print Msg
        print "####### Starting State Below ###################"
        print "Bel_Pr Agent           State_Features"
        print "_____________________________________"
        self.world.printState()
        print "####### Starting State Above ###################"
        print "\n"
        # STACY CHANGED - note they act in parallel so only one step 
        for t in range(1):
            # level deturns amount of details
            self.world.explain(self.world.step(),level=3)
            # print self.world.step()
            # self.world.state[None].select()
            self.world.printState()
            if self.world.terminated():
                break

# helper fcns for dynamics


# setToScaledMatrix(Val_to_set,Val_to_scale,numerator,denominator,knot_size,1) the fourth argument sets knot spacing 


def setToScaledKeyMatrix(SetVar,V,numerator,denominator,inc,i = 1.0): # simple switch function to handle whether numerator is staekey or a constant
    if (isinstance(numerator,int) or isinstance(numerator,float)):
        return setToScaledDenKeyMatrix(SetVar,V,numerator+ inc/2,denominator,inc,i = 1.0) # STACY ADDED THRESHOLD
    else:
        return setToScaledKeysMatrix(SetVar,V,numerator,denominator,inc,i, inc/2) # STACY ADDED THRESHOLD

### This assumes numerator is a python number and denominator is a statekey
def setToScaledDenKeyMatrix(SetVar,V,numerator,denominator,inc,i): # dummbest way (would want line segments or at least a balanced tree)
    if (i < inc):
        return setToConstantMatrix(SetVar,0) # supply insufficient so set N to 0
    else:
        return {'if': multiCompareRow({denominator:i},threshold=numerator), # are scaled cases greater than supply
                True: setToScaledDenKeyMatrix(SetVar,V,numerator,denominator,inc,i-inc),  # recurse with a reduced proportion
                False: setToFeatureMatrix(SetVar,V,i)}  # Scale Nonurgent by i


### This assumes both numerator and denominator are statekeys
def setToScaledKeysMatrix(SetVar,V,numerator,denominator,inc,i = 1.0, threshold = 0): # dummbest way (would want line segments or at least a balanced tree)
    if (i < inc):
        return setToConstantMatrix(SetVar,0) # supply insufficient so set N to 0
    else:
        return {'if': multiCompareRow({denominator:i,numerator:-1.0}, threshold), # are scaled cases greater than supply
                True: setToScaledKeysMatrix(SetVar,V,numerator,denominator,inc,i-inc, threshold),  # recurse with a reduced proportion
                False: setToFeatureMatrix(SetVar,V,i)}  # Scale Nonurgent by i


def setToMinMatrix(Key,Ckey1,Ckey2):
    return {'if': differenceRow(Ckey1,Ckey2,0.0),
            True: setToFeatureMatrix(Key,Ckey2),
            False: setToFeatureMatrix(Key,Ckey1)}

def setToMaxMatrix(Key,Ckey1,Ckey2):
    return {'if': differenceRow(Ckey1,Ckey2,0.0),
            True: setToFeatureMatrix(Key,Ckey1),
            False: setToFeatureMatrix(Key,Ckey2)}


# this function just illustrates the guts of conditional tests
def multiCompareRow(ScaledKeys,threshold=0.0): # scales and sums all the keys and
    # print "scaledcompare", ScaledKeys
    return KeyedPlane(KeyedVector(ScaledKeys), threshold)  #       then tests if > threshold


def multiSetMatrix(Key, ScaledKeys): # scales and sums all the keys in ScaledKeys dict and adds offset if CONSTANT in ScaledKeys
    # print ScaledKeys
    return KeyedMatrix({Key: KeyedVector(ScaledKeys)})  #  then sets Key which may be in ScaledKeys in which case it is adding to scaled value of Key

turnOrder=(set(['H1','H2']),)
supplyAgts = Order(turnOrder)

# Create configuration file
config = SafeConfigParser()
f = open('simp_hospital.cfg','w')
config.write(f)
f.close()

supplyAgts.runit("Hospital")

