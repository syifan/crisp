
'''
Psychsim web site https://github.com/pynadath/psychsim.git
Documentation in doc directory
'''

import sys
from ConfigParser import SafeConfigParser
from argparse import ArgumentParser
import StringIO


from psychsim.pwl import *
from psychsim.reward import *
from psychsim.action import *
from psychsim.world import World,stateKey,actionKey,binaryKey,modelKey,makeFuture
from psychsim.agent import Agent


    # Create scenario
class Supply:

    def __init__(self,turnOrder):

        self.maxRounds=4
        self.world = World()
        SafetyStock = 1
        numHospitals = 2
        numSupplies = 0
        hospital1 = Agent('H1')
        hospital2 = Agent('H2')
        # supplier1 = Agent('S1')
        hospAgts = [hospital1, hospital2]
        # suppAgts = [supplier11]
        totals = {'hInv':30,'sInv':30,'hDem':30,'sDem':30}

        # define Hospital agents
        # Player state, actions and parameters common to all Hospital Agents
        for i in range(numHospitals):
            me = hospAgts[i]
            self.world.addAgent(me)
            # Define the State
            # many of these would be linked to states in the  simulation 
            self.world.defineState(me.name,'Inventory',int,lo=0,hi=totals['hInv'])
            me.setState('Inventory',6)
            self.world.defineState(me.name,'TotalDemand',int,lo=0,hi=totals['hDem'])
            me.setState('TotalDemand',7)
            self.world.defineState(me.name,'UrgentCases',int,lo=0,hi=totals['hDem'])
            me.setState('UrgentCases',4)
            self.world.defineState(me.name,'NonUrgentCases',int,lo=0,hi=totals['hDem'])
            me.setState('NonUrgentCases',3)
            self.world.defineState(me.name,'UrgentSatisfied',int,lo=0,hi=totals['hDem'])
            me.setState('UrgentSatisfied',0)
            self.world.defineState(me.name,'NonUrgentSatisfied',int,lo=0,hi=totals['hDem'])
            me.setState('NonUrgentSatisfied',0)
            self.world.defineState(me.name, 'Supply', int, lo=0.0, hi=100.0)
            me.setState('Supply', 0)
            self.world.defineState(me.name, 'ScaledUrgent', int, lo=0.0, hi=100.0)
            me.setState('ScaledUrgent', 0.0)

            # These dependencies tell PyschSim the order in which to compute variables, for example
            # Compute new value for NonUrgentSatisfied only *after* computing new value for Supply
            # 

            self.world.addDependency(stateKey(me.name, 'NonUrgentSatisfied'), stateKey(me.name, 'TotalDemand'))
            self.world.addDependency(stateKey(me.name, 'NonUrgentSatisfied'), stateKey(me.name, 'Supply'))

            self.world.addDependency(stateKey(me.name, 'ScaledUrgent'), stateKey(me.name, 'TotalDemand'))
            self.world.addDependency(stateKey(me.name, 'ScaledUrgent'), stateKey(me.name, 'Supply'))
            
            self.world.addDependency(stateKey(me.name, 'UrgentSatisfied'), stateKey(me.name, 'ScaledUrgent'))

            self.world.addDependency(stateKey(me.name, 'Inventory'), stateKey(me.name, 'UrgentSatisfied'))
            self.world.addDependency(stateKey(me.name, 'Inventory'), stateKey(me.name, 'NonUrgentSatisfied'))



            # Recipes --- which in PsychSim we can treat as Actions

            Allocate = me.addAction({'verb': 'allocate','recipe':'recipe1'})
            Allocate2 = me.addAction({'verb': 'allocate','recipe':'recipe2'})

            # Parameters
            # Horizon is how far aead agent reasons to determine next action
            me.setHorizon(1)
            # Discount is how much agent discounts future rewards
            me.setAttribute('discount',0.9)

        # this sets the reward for the agent (this is for reference -since this agent has only one action
        # the reward is meaningless

        print "hospital H1 cares more about Urgent"
        hospital1.setReward(maximizeFeature(stateKey(hospital1.name, 'UrgentSatisfied')),4.0)
        hospital1.setReward(maximizeFeature(stateKey(hospital1.name, 'NonUrgentSatisfied')),1.0)
        print "hospital H2 cares more about NonUrgent because they make more money from NonUrgent"
        hospital2.setReward(maximizeFeature(stateKey(hospital2.name, 'UrgentSatisfied')),1.0)
        hospital2.setReward(minimizeFeature(stateKey(hospital2.name, 'NonUrgentSatisfied')),4.0)

        # Levels of belief - for example doe sthe agent have beiefs about anther agent's beliefs about ....
        # - not using theory of mind in this example so this is also for reference only
        # david.setRecursiveLevel(3)
        # stacy.setRecursiveLevel(3)

        #  turnorder is the order in which agents make decisions
        # ['Agent1', 'Agent2', 'Agent3'] means in order
        # [['Agent1', 'Agent2', 'Agent3']] means in parallel
        # and if memory serves
        # [['Agent1', 'Agent2'], 'Agent3'] means 1 and 2 in parallel followed by 3
        # in this example turnOrder is set below when the class is instantiated
        
        self.world.setOrder(turnOrder)


        # Sets the termination condition when running the simulation
        
        # self.world.addTermination(makeTree({'if': thresholdRow(stateKey(None,'round'),self.maxRounds),
        #                           True: True, False: False}))

        # Dynamics of hospital orders

        for h in hospAgts:

            Inventory = stateKey(h.name,'Inventory')
            TotalDemand = stateKey(h.name,'TotalDemand')
            UrgentCases = stateKey(h.name,'UrgentCases')
            NonUrgentCases = stateKey(h.name,'NonUrgentCases')
            UrgentSatisfied = stateKey(h.name,'UrgentSatisfied')
            NonUrgentSatisfied = stateKey(h.name,'NonUrgentSatisfied')
            Supply = stateKey(h.name,'Supply')
            ScaledUrgent = stateKey(h.name,'ScaledUrgent')

            
            atom = Action({'subject': h.name,'verb': 'allocate','recipe': 'recipe1'})
        
            tree = makeTree(KeyedMatrix({TotalDemand: KeyedVector({UrgentCases:1.0,NonUrgentCases:1.0})}))
            self.world.setDynamics(TotalDemand,atom,tree)

            tree = makeTree({'if': differenceRow(Inventory, TotalDemand, SafetyStock),
                             True: setToFeatureMatrix(UrgentSatisfied,UrgentCases),
                             False: {'if': greaterThanRow(Inventory,UrgentCases),
                                     True: setToFeatureMatrix(UrgentSatisfied, UrgentCases),
                                     False: {'if': thresholdRow(Inventory,0),
                                             True: setToFeatureMatrix(UrgentSatisfied, Inventory),
                                             False: setToConstantMatrix(UrgentSatisfied, 0)}}})
            self.world.setDynamics(UrgentSatisfied,atom,tree)

            tree = makeTree({'if': differenceRow(Inventory, TotalDemand, SafetyStock),
                             True: setToFeatureMatrix(NonUrgentSatisfied,NonUrgentCases),
                             False: {'if': differenceRow(Inventory,UrgentCases, SafetyStock),
                                     True: multiSetMatrix(NonUrgentSatisfied, {Inventory:1.0, UrgentCases:-1.0,CONSTANT: -1.0 * SafetyStock}),
                                     False: setToConstantMatrix(NonUrgentSatisfied, 0)}})
            self.world.setDynamics(NonUrgentSatisfied,atom,tree)

            # Inventory
            tree = makeTree(multiSetMatrix(Inventory, {Inventory:1.0,NonUrgentSatisfied:-1.0,UrgentSatisfied:-1.0}))
            self.world.setDynamics(Inventory,atom,tree)

            # RECIPE2
            atom = Action({'subject': h.name,'verb': 'allocate','recipe': 'recipe2'})

            # uses proportional e.g.,  U = U * ((Inv-T)/(N+U) SO CREATE FOLOWING INTERMEDIATE StateKey VALUES
            # for the denominator and numerator e.g., TotalDemand = N+U and Supppy = Inv-T
            # ScaledUrgent for  U * ((Inv-T)/(N+U)

            tree = makeTree(KeyedMatrix({TotalDemand: KeyedVector({UrgentCases:1.0,NonUrgentCases:1.0})}))
            self.world.setDynamics(TotalDemand,atom,tree)
            tree = makeTree(setToFeatureMatrix(Supply,Inventory, 1.0,-1.0 * SafetyStock))
            self.world.setDynamics(Supply,atom,tree)
            tree = makeTree(setToScaledMatrix(ScaledUrgent,UrgentCases,Supply,TotalDemand,.1))
            self.world.setDynamics(ScaledUrgent,atom,tree)
 
            # Nonurgent cases
            tree = makeTree({'if': multiCompareRow({NonUrgentCases:1,UrgentCases:1,Supply:-1.0},0.0),
                             False: setToFeatureMatrix(NonUrgentSatisfied,NonUrgentCases),
                             True: {'if': thresholdRow(Supply,0),
                                     True: setToScaledMatrix(NonUrgentSatisfied,NonUrgentCases,Supply,TotalDemand,.1),
                                     False: setToConstantMatrix(NonUrgentSatisfied,0)}})
            self.world.setDynamics(NonUrgentSatisfied,atom,tree)
    
            # Urgent cases
            tree = makeTree({'if': multiCompareRow({NonUrgentCases:1,UrgentCases:1,Supply:-1},0.0),
                             False: setToFeatureMatrix(UrgentSatisfied,UrgentCases),
                             True: {'if': thresholdRow(Supply,0),
                                     True: {'if': multiCompareRow({UrgentCases:1,ScaledUrgent:-1},SafetyStock),
                                            True: setToFeatureMatrix(UrgentSatisfied, ScaledUrgent,1.0,SafetyStock),
                                            False: setToFeatureMatrix(UrgentSatisfied, UrgentCases)},
                                     False: setToMinMatrix(UrgentSatisfied, UrgentCases, Inventory)}})
            self.world.setDynamics(UrgentSatisfied,atom,tree)

            # Inventory
            tree = makeTree(multiSetMatrix(Inventory, {Inventory:1.0,NonUrgentSatisfied:-1.0,UrgentSatisfied:-1.0}))
            self.world.setDynamics(Inventory,atom,tree)


            

        # not using theory of mind in this example for hospitals:
        # BUT if we were we could define multiple models for Hospital1 which would either represent the True model of Hospital1
        #    h.addModel(True, R={}, level=3, rationality=10, horizon=3)
        #    h.setReward(maximizeFeature(stateKey(me.name, 'UrgentSatisfied')),4.0, True)
        #    h.setReward(maximizeFeature(stateKey(me.name, 'NonUrgentSatisfied')),1.0, True)
        # or other models that another agent may have of Hospital1
        #    h.addModel('Faulty', R={}, level=3, rationality=10, horizon=3)
        #    h.setReward(minimizeFeature(stateKey(me.name, 'UrgentSatisfied')),1.0, 'Faulty')
        #    h.setReward(minimizeFeature(stateKey(me.name, 'NonUrgentSatisfied')),1.0, 'Faulty')
        # Then we could give another agent a belief distribtuion over those models
        #    belief = {True:0.4,'Faulty':0.6}
        #    self.world.setMentalModel('Hospital2','Hospital1',belief)
        # which Hospital2 would use and update in the face of evidence

    def runit(self,Msg):

        print Msg
        print "####Starting State#######:"
        print "Bel_Pr Agent           State_Features"
        print "_____________________________________"
        self.world.printState()
        print "##########################"
        print "####\n"

        for t in range(2):
            # level deturns amount of details
            self.world.explain(self.world.step(),level=3)
            # print self.world.step()
            # self.world.state[None].select()
            self.world.printState()
            if self.world.terminated():
                break        

# helper fcns for dynamics
# setToScaleMatrix(V,supply,cases,.1,1) the fourth argument sets knot spacing

def setToScaledMatrix(SetVar,V,numerator,denominator,inc,i = 1.0): # dummbest way (would want line segments or at least a balanced tree)
    # print SetVar,V,numerator,denominator,i,inc
    if (i < inc):
        return setToConstantMatrix(SetVar,0) # supply insufficient so set N to 0
    else:
        return {'if': multiCompareRow({denominator:i,numerator:-1.0}), # are scaled cases greater than supply
                True: setToScaledMatrix(SetVar,V,numerator,denominator,inc,i-inc),  # recurse with a reduced proportion
                False: setToFeatureMatrix(SetVar,V,i)}  # Scale Nonurgent by i


def setToMinMatrix(Key,Ckey1,Ckey2):
    return {'if': differenceRow(Ckey1,Ckey2,0.0),
            True: setToFeatureMatrix(Key,Ckey2),
            False: setToFeatureMatrix(Key,Ckey1)}

def setToMaxMatrix(Key,Ckey1,Ckey2):
    return {'if': differenceRow(Ckey1,Ckey2,0.0),
            True: setToFeatureMatrix(Key,Ckey1),
            False: setToFeatureMatrix(Key,Ckey2)}


# this function just illustrates the guts of conditional tests
def multiCompareRow(ScaledKeys,threshold=0.0): # scales and sums all the keys and
    # print "scaledcompare", ScaledKeys
    return KeyedPlane(KeyedVector(ScaledKeys), threshold)  #       then tests if > threshold

           
def multiSetMatrix(Key, ScaledKeys): # scales and sums all the keys in ScaledKeys dict and adds offset if CONSTANT in ScaledKeys
    # print ScaledKeys
    return KeyedMatrix({Key: KeyedVector(ScaledKeys)})  #  then sets Key which may be in ScaledKeys in which case it is adding to scaled value of Key

turnOrder=['H1','H2']
supplyAgts = Supply(turnOrder)

# Create configuration file
config = SafeConfigParser()
f = open('simp_hospital.cfg','w')
config.write(f)
f.close()

supplyAgts.runit("Hospital")

