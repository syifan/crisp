from psychsim_agents.psychsim_agent import PsychSimAgent
from psychsim.action import Action


class Recipe(object):
    """
    A recipe defines the possible actions that can be selected by psychsim decision maker
    """

    def __init__(self, name="Recipe"):
        self.name = name

    def register_recipe(self, agent, action):
        """
        Each recipe has to implement the register recipe function by creating the dynamics tree updating the features.
        :param PsychSimAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        pass
