import itertools


class Order(object):
    """ Order is a message sent from an downstream node to upstream node to
        buy some drug
    """

    new_id = itertools.count().next

    def __init__(self):
        self.id = Order.new_id()

        self.src = None  # source
        self.dst = None  # destination
        self.amount = int(0)
        self.place_time = 0  # the time that order was placed

        self.delivery = []

        self.recv_time = 0  # the actual time that this order was received by destination (we have information lead
        # time)
        self.exp_recv_time = 0  # expected received time (=place_time+information lead time)
        self.expire_time = 0  # how far in future this order will expire, if not satisfied (at period
        # place_time + expire_time the order will expire)
